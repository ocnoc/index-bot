declare var socket : any;
declare var lobby : any;
declare var viewChanger : any;
declare var hostModal : any;
declare var roomBrowser : any;
function Room(settings, host, hostAvatar, id, numberOfPlayers, numberOfSpectators, players, inLobby, songLeft) {
    return {settings, host, hostAvatar, id, numberOfPlayers, numberOfSpectators, players, inLobby, songLeft}
}

class IndexbotQuickplay implements IndexbotModule {
    common: IndexbotCommon;
    settingsModule: IndexbotSettings;
    base: Indexbot;
    settings: any;
    rooms: any[];
    BUTTON_TEXT: string;
    BUTTON_ID: string;
    constructor() {
        /** @type {IndexbotCommon} */
        this.common;
        /**@type {IndexbotSettings} */
        this.settingsModule;
        this.base = indexbot_base;
        this.settings;
        this.rooms = [];
        this.BUTTON_TEXT = "Quick Play";
        this.BUTTON_ID = "indexbotQuickplayButton";
    }
    preInit: () => void;
    /**
     * Clears the saved room list.
     */
    _clearRooms() {
        this.rooms = [];
        this.common.debug(this, "Rooms cleared!");
    }
    /**
     * Returns a promise that gets fulfilled once the room list is updated.
     */
    _updateRoomList() {
        return new Promise<void>((resolve, reject) => {
            let roomListListener = new Listener("New Rooms", (rooms) => {
                this._clearRooms();
                rooms.forEach((room) => {
                    this.rooms.push(Room(room.settings, room.host, room.hostAvatar, room.id, room.numberOfPlayers, room.numberOfSpectators, room.players, room.inLobby, room.songLeft));
                });
                roomListListener.unbindListener();
                socket.sendCommand({
                    type: 'roombrowser',
                    command: 'remove roombrowser listners'
                });
                resolve();
            });
            roomListListener.bindListener();
            socket.sendCommand({
                type: "roombrowser",
                command: "get rooms"
            });
        });
    }
    /**
     * Looks through the loaded room list for a specific room name, and returns the found room object or undefined if not found.
     * @param {String} name Name of the room
     */
    async _findRoomByName(name) {
        await this._updateRoomList();
        let rooms = this.rooms;
        let target;
        rooms.forEach(function (room) {
            if (room.settings.roomName == name) {
                target = room;
            }
        });
        return target;
    }
    /**
     * Joins a specified room name with a specified (or unspecified) password. Will spectate if in a game.
     * @param {String} name Name of the room to join
     * @param {*} password A string if joining with a password, otherwise should be undefined
     */
    async _joinRoom(name, password) {
        let room = await this._findRoomByName(name);
        if (room == undefined) {
            throw "room not found";
        }
        if (room.inLobby) {
            roomBrowser.fireJoinLobby(room.id, password);
        }
        else {
            roomBrowser.fireSpectateGame(room.id, password);
        }
    }
    /**
     * Hosts a room with the given settings
     * @param {String} settingsString Encoded settings given by the game
     * @param {String} name Room name
     * @param {*} password Password to use
     */
    _hostRoom(settingsString, name, password) {
        const GAMEMODE = "Multiplayer";
        const COMMAND = "host room";
        let roomName = name;
        let isprivate = password != "";
        let settings = hostModal._settingStorage._serilizer.decode(settingsString);
        let gamemode = GAMEMODE;
        let command = COMMAND;
        // Save codes dont include room name, password or gamemode. Using standard gamemode to save time for now
        settings.roomName = roomName;
        settings.privateRoom = isprivate;
        settings.gameMode = gamemode;
        settings.password = password;

        let hostListener = new Listener("Host Game", function (response) {
            lobby.setupLobby(response, false);
            viewChanger.changeView("lobby");
            hostListener.unbindListener();
        }.bind(this));

        hostListener.bindListener();
        socket.sendCommand({
            type: 'roombrowser',
            command: command,
            data: settings
        });
    }
    _quickHost() {
        if (this.settings.roomcode.getValue() == "") {
            this.common.alert("Please set up room settings first in Indexbot settings", "error");
            return false;
        }
        this._hostRoom(this.settings.roomcode.getValue(), this.settings.roomname.getValue(), this.settings.roompassword.getValue());
        this.common.closeLoadingAlert();
    }
    /**
     * Master quickplay function. looks up the room stored. If it exists, tries to join. Otherwise tries to host it.
     */
    _quickPlay() {
        if (this.settings.roomname.getValue() == "") {
            this.common.alert("Please set up a room name in settings first", "error");
            return false;
        }
        this.common.loadingAlert("Querying room data...");
        let name = this.settings.roomname.getValue();
        let password = this.settings.roompassword.getValue() == "" ? undefined : this.settings.roompassword.getValue();
        this._joinRoom(name, password).then(this.common.closeLoadingAlert, this._quickHost.bind(this));
    }
    /**
     * Adds the quick play button to the main menu.
     */
    _addQuickPlayButton() {
        let mainMenu = $("#mainMenu");
        let quickPlayButton = document.createElement("div");
        let quickPlayLabel = document.createElement("h1");
        let module = this; // Gets around 'this' being reassigned in onclick functions
        quickPlayLabel.textContent = this.BUTTON_TEXT;
        quickPlayButton.id = this.BUTTON_ID;
        quickPlayButton.classList.add("button", "floatingContainer", "mainMenuButton");
        quickPlayButton.onclick = function () {
            module._quickPlay();
        };
        quickPlayButton.appendChild(quickPlayLabel);
        mainMenu.append(quickPlayButton);
    }
    init(dependencies) {
        this.base.injectDependency(this, dependencies, "common");
        this.base.injectDependency(this, dependencies, "settings", "settingsModule");
        this.settingsModule.registerSetting({ module: this, name: "Room Name", description: "The name of the room to host or join", id: "roomname", value: "", type: this.settingsModule.SETTING_TYPE.STRING, onchange: null });
        this.settingsModule.registerSetting({ module: this, name: "Room Password", description: "The password of the room to host or join", id: "roompassword", value: "", type: this.settingsModule.SETTING_TYPE.STRING, onchange: null });
        this.settingsModule.registerSetting({ module: this, name: "Settings Code", description: "The room settings code. Get this from the load settings menu", id: "roomcode", value: "", type: this.settingsModule.SETTING_TYPE.STRING, onchange: null });
    }
    async postInit() {
        this.settings = await this.settingsModule.getSettings(this);
        this.common.includeExternalCSS(this, "https://gl.githack.com/ocnoc/index-bot/-/raw/master/modules/css/quickplay.css");

        this._addQuickPlayButton();

    }
}










indexbot_base.registerModule("Quickplay", "quickplay", "Adds a button to the main menu to quickly host or join a match", new IndexbotQuickplay(), ["settings", "common"]);