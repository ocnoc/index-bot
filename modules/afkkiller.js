function IndexbotAfkKiller() {
    this.settingsModule;
    this.settings;
    this.base = indexbot_base;
}

IndexbotAfkKiller.prototype._createAfkTimeoutOverride = function(setting, original) {
    return function() {
        if(!setting.getValue()) {
            return original.apply(afkKicker);
        }
        clearTimeout(afkKicker.afkWarningTimeout);
    }
}

IndexbotAfkKiller.prototype._createHostTimeoutOverride = function(setting, original) {
    return function() {
        if(!setting.getValue()) {
            return original.apply(afkKicker);
        }
        clearTimeout(afkKicker.hostAfkWarningTimeout);
    }
}

IndexbotAfkKiller.prototype.init = function(dependencies) {
    this.base.injectDependency(this, dependencies, "settings", "settingsModule");
    this.settingsModule.registerSetting(this, "AFK Timer Killer", "Controls whether the AFK timer should be killed", "afktimerkiller", false, this.settingsModule.SETTING_TYPE.BOOL, null);
}

IndexbotAfkKiller.prototype.postInit = async function() {
    this.settings = await this.settingsModule.getSettings(this);

    let originalAfkTimeout = afkKicker.setupAfkTimeout;
    let originalHostTimeout = afkKicker.setupHostTimeout;

    afkKicker.setupAfkTimeout = this._createAfkTimeoutOverride(this.settings.afktimerkiller, originalAfkTimeout);
    afkKicker.setupHostTimeout = this._createHostTimeoutOverride(this.settings.afktimerkiller, originalHostTimeout);
}

indexbot_base.registerModule("AFK Timer Killer", "afkkiller", "Disables the AFK timers", new IndexbotAfkKiller(), ["settings"]);