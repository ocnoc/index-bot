(function () {
    /**
     * @class
     * @alias IndexbotHTML
     */
    class IndexbotHTML {
        constructor() {
            this.common;
            this.base = indexbot_base;
        }
        _DOMIDSet(element, id) {
            if (!id) {
                return;
            }
            element.id = id;
        }
        createTable(options) {
            const headID = options.headID;
            const bodyID = options.bodyID;
            const id = options.id;
            const parent = options.parent;


            let table = document.createElement("table");
            let head = document.createElement("thead");
            let body = document.createElement("tbody");

            this._DOMIDSet(table, id);
            this._DOMIDSet(head, headID);
            this._DOMIDSet(body, bodyID);

            table.appendChild(head);
            table.appendChild(body);
            table.classList.add("table");

            if (parent) {
                parent.append(table);
            }

            return table;
        }
        createAccordionContainer(options) {
            const id = options.id;
            const parent = options.parent;

            let accordion = document.createElement("div");

            this._DOMIDSet(accordion, id);
            accordion.classList.add("panel-group");
            if (parent) {
                parent.append(accordion);
            }

            return accordion;
        }
        createAccordionElement(options) {
            const title = options.title;
            const elementID = options.elementID;
            const headerID = options.headerID;
            const bodyID = options.bodyID;
            const parentID = options.parentID;

            let card = document.createElement("div");
            let header = document.createElement("div");
            let headerTitle = document.createElement("h5");
            let headerTitleButton = document.createElement("button");
            let collapseContainer = document.createElement("div");

            this._DOMIDSet(card, elementID);
            card.classList.add("panel", "indexbot-panel");

            this._DOMIDSet(header, headerID);
            header.classList.add("panel-heading");

            headerTitle.classList.add("panel-title");

            headerTitleButton.classList.add("btn", "btn-info", "btn-block");
            headerTitleButton.type = "button";
            headerTitleButton.setAttribute("data-toggle", "collapse");
            headerTitleButton.setAttribute("data-target", `#${bodyID}`);
            headerTitleButton.setAttribute("aria-expanded", true);
            headerTitleButton.setAttribute("aria-controls", bodyID);
            headerTitleButton.textContent = title;

            this._DOMIDSet(collapseContainer, bodyID);
            collapseContainer.classList.add("collapse");
            collapseContainer.setAttribute("aria-labelledby", headerID);
            if (parentID) {
                collapseContainer.setAttribute("data-parent", `#${parentID}`);
            }

            card.appendChild(header);
            card.appendChild(collapseContainer);
            header.appendChild(headerTitle);
            headerTitle.appendChild(headerTitleButton);

            if (parentID) {
                $(`#${parentID}`).append(card);
            }

            return card;
        }
        createModalWindow(options) {
            const title = options.title;
            const windowID = options.windowID;
            const headerID = options.headerID;
            const bodyID = options.bodyID;
            const insertIntoDOM = options.insertIntoDOM || true;

            let parentContainer = $("#gameContainer");
            let newModal = document.createElement("div");
            let newModalDialog = document.createElement("div");
            let newModalContent = document.createElement("div");
            let newModalHeader = document.createElement("div");
            let newModalBody = document.createElement("div");

            let newModalHeaderTitle = document.createElement("h2");

            newModal.appendChild(newModalDialog);
            newModalDialog.appendChild(newModalContent);

            newModalHeader.appendChild(newModalHeaderTitle);

            newModalContent.appendChild(newModalHeader);
            newModalContent.appendChild(newModalBody);

            if (insertIntoDOM) {
                parentContainer.append(newModal);
            }

            this._DOMIDSet(newModal, windowID);
            this._DOMIDSet(newModalHeader, headerID);
            this._DOMIDSet(newModalBody, bodyID);

            newModal.classList.add("modal", "fade");
            newModal.setAttribute("tabindex", "-1");
            newModal.role = "dialog";

            newModalDialog.classList.add("modal-dialog");
            newModalDialog.role = "document";

            newModalContent.classList.add("modal-content");

            newModalBody.classList.add("modal-body");

            newModalHeader.classList.add("modal-header");

            newModalHeaderTitle.classList.add("modal-title");
            newModalHeaderTitle.textContent = title;

            return newModal;
        }
        addRoundOptionButton(options) {
            const faIconClass = options.faIconClass;
            const tooltip = options.tooltip;
            const onclick = options.onclick;
            const id = options.id;

            let optionContainer = $("#qpOptionContainer >");
            let newButton = document.createElement("div");
            let newButtonIcon = document.createElement("i");

            newButton.classList.add("clickAble");
            newButton.classList.add("qpOption");
            newButton.classList.add("indexbot-qpicon-width");
            newButton.onclick = onclick;
            newButton.setAttribute("data-toggle", "popover");
            newButton.setAttribute("data-content", tooltip);
            newButton.setAttribute("data-trigger", "hover");
            newButton.setAttribute("data-placement", "bottom");
            newButton.setAttribute("data-original-title", "");
            newButton.setAttribute("title", "");
            this._DOMIDSet(newButton, id);

            newButtonIcon.classList.add("fa");
            newButtonIcon.classList.add("qpMenuItem");
            newButtonIcon.classList.add(faIconClass);
            newButtonIcon.setAttribute("aria-hidden", "true");

            newButton.appendChild(newButtonIcon);
            optionContainer.append(newButton);

            // Initialize popovers for all indexbot qpoptions.
            $(function () {
                $('.indexbot-qpicon-width').popover();
            });

            return newButton;
        }
        addOptionButton(options) {
            const title = options.title;
            const buttonID = options.buttonID;
            const onclick = options.onclick;


            let buttonList = $("#optionsContainer >");
            let newButton = document.createElement("li");

            newButton.classList.add("clickAble");
            newButton.onclick = onclick;
            this._DOMIDSet(newButton, buttonID);
            newButton.textContent = title;

            buttonList.prepend(newButton);

            return newButton;
        }
        init(dependencies) {
            this.base.injectDependency(this, dependencies, "common");
            this.common.includeExternalCSS(this, "https://gl.githack.com/ocnoc/index-bot/-/raw/master/modules/css/html.css");
        }
    }

    
    
    
    
    

    
    
    indexbot_base.registerModule("Indexbot HTML", "html", "Common functions for creating HTML elements on AMQ", new IndexbotHTML(), ["common"]);
})()
