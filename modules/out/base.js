class ModuleDescriptor {
    constructor(name, id, description, module, requiredModules, optionalModules) {
        this.name = name;
        this.id = id;
        this.description = description;
        this.module = module;
        this.requiredModules = requiredModules;
        this.optionalModules = optionalModules;
    }
}
/**
 * Contains the basic module and setting framework for Indexbot. No database functionality by default.
 * @constructor
 */
class Indexbot {
    constructor() {
        this.modules = {};
        this.states = {
            LOADING: 0,
            PREINIT: 1,
            INIT: 2,
            POSTINIT: 3,
            FINISHED: 4
        };
        this.COMMON_HOOK_TARGETS = {
            PREINIT: "preInit",
            INIT: "init",
            POSTINIT: "postInit"
        };
        this.state = this.states.LOADING;
    }
    /**
     *
     * @param {Object} logger Module that sends the error
     * @param {*} message Message to print into console
     */
    error(logger, message) {
        console.error(`${logger.descriptor().name}: ${message}`);
    }
    /**
     *
     * @param {Object} logger Module that sends the log
     * @param {*} message Message to print into console
     */
    log(logger, message) {
        console.log(`${logger.descriptor().name}: ${message}`);
    }
    /**
     * Checks if a given module id already exists. If not, registers a new module internally, and sets the module's descriptor function.
     * @param {String} name Name of the module
     * @param {String} id A backend ID to identify the module
     * @param {String} description A description of the module
     * @param {Object} module The javascript object that this module is associated with. Should have a constructor so we can set a descriptor function.
     * @param {Array} requiredModules Required modules for this module. Optional.
     */
    registerModule(name, id, description, module, requiredModules = null, optionalModules = null) {
        let match = Object.keys(this.modules).filter((m) => m == id);
        if (match.length >= 1) {
            this.error(this, `Error registering module ${id}: id already registered.`);
            return;
        }
        let newModule = new ModuleDescriptor(name, id, description, module, requiredModules, optionalModules);
        module.constructor.prototype.descriptor = function () {
            return newModule;
        };
        this.modules[id] = newModule;
        if (typeof module.constructor.prototype.preInit != 'function') {
            module.constructor.prototype.preInit = function () { };
        }
        if (typeof module.constructor.prototype.init != 'function') {
            module.constructor.prototype.init = function () { };
        }
        if (typeof module.constructor.prototype.postInit != 'function') {
            module.constructor.prototype.postInit = function () { };
        }
    }
    /**
     * Looks for a module and returns it if it's found. Otherwise returns null.
     * @param {String} id Id of the module to get
     */
    getModule(id) {
        let match = Object.keys(this.modules).filter((m) => m == id);
        if (match.length < 1) {
            this.error(this, `No module exists with id ${id}`);
            return null;
        }
        return this.modules[match[0]].module;
    }
    /**
     * Looks for a module descriptor and returns it if it's found. Otherwise returns null.
     * @param {String} id Id of the module to get
     */
    getModuleDescriptor(id) {
        let match = Object.keys(this.modules).filter((m) => m == id);
        if (match.length < 1) {
            this.error(this, `No module exists with id ${id}`);
            return null;
        }
        return this.modules[match[0]];
    }
    /**
     * Returns all modules loaded.
     */
    getModules() {
        return Object.values(this.modules);
    }
    /**
     * Runs a module's preinit, allowing it to set up any necessary hooks and avoid circular dependencies.
     * Modules should assume all they can access are the base module in this function.
     * @param {Object} descriptor Descriptor for the module being initialized
     */
    async preInitializeModule(descriptor) {
        let module = descriptor.module;
        try {
            await module.preInit();
        }
        catch (e) {
            this.error(this, `Module ${module.descriptor().name} failed to pre-initialize due to the following error.`);
            this.error(module, e);
            throw e;
        }
    }
    /**
     * Initializes a given module, it will expect all it's requirements to at least be pre-initialized and loaded.
     * @param {ModuleDescriptor} moduleDescriptor Descriptor for the module to be initialized
     */
    async initializeModule(moduleDescriptor) {
        let module = moduleDescriptor.module;
        let requiredModules = moduleDescriptor.requiredModules;
        let optionalModules = moduleDescriptor.optionalModules;
        let id = moduleDescriptor.id;
        let modulesToSend;
        if (requiredModules) {
            modulesToSend = requiredModules.map(this.getModuleDescriptor.bind(this));
            if (optionalModules) {
                optionalModules.forEach(m => {
                    if (this.modules[m]) {
                        modulesToSend.push(this.modules[m]);
                    }
                });
            }
            let loadedModules = Object.keys(this.modules);
            requiredModules.forEach(m => {
                if (!loadedModules.includes(m)) {
                    this.error(this, `Error initializing module ${id}: missing required module ${m}`);
                    throw false;
                }
            });
        }
        ;
        try {
            await module.init(modulesToSend);
        }
        catch (e) {
            this.error(this, `Module ${moduleDescriptor.name} failed to load due to the following error.`);
            this.error(module, e);
            throw e;
        }
    }
    /**
     * Postinitializes a module. This is where intermodule interaction should go, if there is any.
     * @param {Object} module The module to postinitialize
     */
    async postInitializeModule(moduleDescriptor) {
        let module = moduleDescriptor.module;
        try {
            await module.postInit();
        }
        catch (e) {
            this.error(this, `Module ${moduleDescriptor.name} failed to load due to the following error.`);
            this.error(module, e);
            throw e;
        }
    }
    /**
     * Registers a function to be called before the target function in the target module.
     * @param {Object} module The module registering the hook
     * @param {Object} targetModule The target module being hooked into
     * @param {Function} targetFunction The name of the function being hooked into
     * @param {Function} hook The function to call before the target
     */
    registerHook(module, targetModule, targetFunction, hook) {
        if (typeof targetModule[targetFunction] != 'function') {
            this.error(module, `Failed to hook into [${targetModule.descriptor().name}]'s function, ${targetFunction}.`);
            return;
        }
        let original = targetModule[targetFunction];
        targetModule[targetFunction] = function () {
            hook.apply(module, arguments);
            return original.apply(targetModule, arguments);
        };
    }
    /**
     * Registers a function to be called before the target function in the target module. Uses await, and makes the wrapper function async.
     * @param {Object} module The module registering the hook
     * @param {Object} targetModule The target module being hooked into
     * @param {Function} targetFunction The name of the function being hooked into
     * @param {Function} hook The function to call before the target
     */
    async registerAwaitedHook(module, targetModule, targetFunction, hook) {
        if (typeof targetModule[targetFunction] != 'function') {
            this.error(module, `Failed to hook into [${targetModule.descriptor().name}]'s function, ${targetFunction}.`);
            return;
        }
        let original = targetModule[targetFunction];
        targetModule[targetFunction] = async function () {
            await hook.apply(module, arguments);
            return await original.apply(targetModule, arguments);
        };
    }
    /**
     * Places a given dependency from a list into a specific property of the calling module.
     * @param {Module} module The module to inject
     * @param {Array} dependencies Array of module descriptors
     * @param {String} dependency Moduleid of the target dependency
     * @param {String} property The property to store the dependency module
     */
    injectDependency(module, dependencies, dependency, property = dependency) {
        dependencies.filter(moduled => moduled.id == dependency).forEach(moduled => module[property] = moduled.module);
    }
    /**
     * Runs every module's init and postInit functions
     */
    async start() {
        if ($("#startPage").length == 1) {
            this.log(this, "Refusing to start, not logged in.");
            return;
        }
        this.state = this.states.PREINIT;
        for (const key in this.modules) {
            let descriptor = this.modules[key];
            await this.preInitializeModule(descriptor);
        }
        this.state = this.states.INIT;
        for (const key in this.modules) {
            let descriptor = this.modules[key];
            await this.initializeModule(descriptor);
        }
        ;
        this.state = this.states.POSTINIT;
        for (const key in this.modules) {
            let descriptor = this.modules[key];
            await this.postInitializeModule(descriptor);
        }
        this.state = this.states.FINISHED;
    }
}
window.indexbot_base = new Indexbot();
const indexbot_base = window.indexbot_base;
indexbot_base.registerModule("Indexbot Base", "base", "Framework for indexbot modules", indexbot_base);
