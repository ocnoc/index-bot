class IndexbotAfkKiller {
    constructor() {
        this.settingsModule;
        this.settings;
        this.base = indexbot_base;
    }
    _createAfkTimeoutOverride(setting, original) {
        return function () {
            if (!setting.getValue()) {
                return original.apply(afkKicker);
            }
            clearTimeout(afkKicker.afkWarningTimeout);
        };
    }
    _createHostTimeoutOverride(setting, original) {
        return function () {
            if (!setting.getValue()) {
                return original.apply(afkKicker);
            }
            clearTimeout(afkKicker.hostAfkWarningTimeout);
        };
    }
    init(dependencies) {
        this.base.injectDependency(this, dependencies, "settings", "settingsModule");
        this.settingsModule.registerSetting({ module: this, name: "AFK Timer Killer", description: "Controls whether the AFK timer should be killed", id: "afktimerkiller", value: false, type: this.settingsModule.SETTING_TYPE.BOOL, onchange: null });
    }
    async postInit() {
        this.settings = await this.settingsModule.getSettings(this);
        let originalAfkTimeout = afkKicker.setupAfkTimeout;
        let originalHostTimeout = afkKicker.setupHostTimeout;
        afkKicker.setupAfkTimeout = this._createAfkTimeoutOverride(this.settings.afktimerkiller, originalAfkTimeout);
        afkKicker.setupHostTimeout = this._createHostTimeoutOverride(this.settings.afktimerkiller, originalHostTimeout);
    }
}
indexbot_base.registerModule("AFK Timer Killer", "afkkiller", "Disables the AFK timers", new IndexbotAfkKiller(), ["settings"]);
