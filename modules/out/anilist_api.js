class IndexbotAnilistAPI {
    constructor() {
        this.base = indexbot_base;
        /**@type {IndexbotCommon} */
        this.common;
    }
    /**
     * Raw anilist query. Returns a promise that gets fulfilled when a response is received.
     * @param {String} query The query to send
     * @param {*} variables Object matching variable names to their values
     */
    async _anilistQuery(query, variables) {
        const url = 'https://graphql.anilist.co';
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            body: JSON.stringify({
                query: query,
                variables: variables
            })
        };
        let response;
        let json;
        response = await fetch(url, options);
        json = await response.json();
        if (!response.ok) {
            throw json;
        }
        return json.data;
    }
    /**
     * Looks up a specified anime anme on Anilist and returns the id associated with it.
     * Will throw an error if the anime can't be found. Can also return the wrong ID if multiple results come up.
     * Remember, this is a best effort. Anime names aren't unique, especially in a series.
     * Uses common to scrub the name of weird characters.
     * @param {String} animeName The anime name to look up
     */
    async getAnilistAnimeID(animeName) {
        let animeNameStripped = this.common.stripAccents(animeName);
        const query = `query ($search: String) { 
        Media (search: $search, type: ANIME) {
            id
        }
    }`;
        // We put animeName as the search query.
        const variables = {
            search: animeNameStripped
        };
        try {
            let data = await this._anilistQuery(query, variables);
            return data.Media.id;
        }
        catch (e) {
            throw e;
        }
    }
    /**
     * Finds an image for the given anime id and fulfills a promise with a link to it.
     * @param {int} animeId the ID of the anime to find an image for
     */
    async getAnilistImage(animeId) {
        const query = `query ($id: Int) {
        Media (id: $id, type: ANIME) {
            coverImage {
                extraLarge
            }
        }
    }`;
        var variables = {
            id: animeId
        };
        try {
            let data = await this._anilistQuery(query, variables);
            return data.Media.coverImage.extraLarge;
        }
        catch (e) {
            throw e;
        }
    }
    init(dependencies) {
        this.base.injectDependency(this, dependencies, "common");
    }
}
indexbot_base.registerModule("Anilist API", "anilistapi", "A set of functions for quickly sending common requests to Anilist.", new IndexbotAnilistAPI(), ["common"]);
