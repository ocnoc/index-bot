/**
 * Legacy module for inserting the anime's image as the quiz background.
 * Legacy because it's mostly a copy paste from railgun. Cleaner than the original indexbot version (no more exact stylesheet id constant),
 * but could use a cleanup.
 */
class IndexbotAnimeAsBGLegacy {
    constructor() {
        /** @type {IndexbotSettings} */
        this.settingsModule;
        this.settings;
        /** @type {IndexbotAnilistAPI} */
        this.anilistapi;
        /** @type {IndexbotCommon} */
        this.common;
        this.base = indexbot_base;
        this.animeAsBGDiv;
        this.game_stylesheet;
        this.game_styleRuleID;
    }
    _addAnimeImageBGDiv() {
        let targetDiv = $("#qpOptionContainer");
        let newDiv = document.createElement("div");
        this.animeAsBGDiv = newDiv;
        newDiv.id = "indexbotAnilistImage";
        targetDiv.after(newDiv);
    }
    _addStyles() {
        // Adding rule for anime as background. Empty to start.
        const stylesheet_id = document.styleSheets.length - 1;
        const stylesheet = document.styleSheets[stylesheet_id];
        const styleRuleID = stylesheet.insertRule(`
    #indexbotAnilistImage {
    }`, stylesheet.rules.length);
        this.game_stylesheet = stylesheet;
        this.game_styleRuleID = styleRuleID;
    }
    _initAnimeAsBGListeners() {
        this.animeAsBGRemoveListener = new Listener("play next song", data => {
            this._clearGameBG();
        });
        this.animeAsBGRemoveListenerQuizEnd = new Listener("quiz over", result => {
            this._clearGameBG();
        });
        this.animeAsBGRemoveListenerGameStart = new Listener("Game Starting", result => {
            this._clearGameBG();
        });
        this.animeAsBGListener = new Listener("answer results", async (results) => {
            if (!this.settings || !this.settings.animeasbglegacy_animeasbg.getValue()) {
                return;
            }
            let songData = results.songInfo;
            let anime = this.common.getAnimeName(songData.animeNames);
            let anilistID = await this.anilistapi.getAnilistAnimeID(anime);
            let imageURL = await this.anilistapi.getAnilistImage(anilistID);
            this._setGameBG(imageURL);
        });
        this.animeAsBGRemoveListener.bindListener();
        this.animeAsBGRemoveListenerQuizEnd.bindListener();
        this.animeAsBGListener.bindListener();
        this.animeAsBGRemoveListenerGameStart.bindListener();
    }
    /**
     * Should be looked at and cleaned up along with clearGameBG, no idea which of these settings are necessary.
     * Should also look into making the blur a changeable setting.
     * @param {string} url URL to set the bakcground image to
     */
    _setGameBG(url) {
        const style = this.game_stylesheet.cssRules[this.game_styleRuleID].style;
        style["background-image"] = "url(\"" + url + "\")";
        style["background-size"] = "75% 100%";
        style["background-attachment"] = "fixed";
        style["background-position"] = "0px";
        style["filter"] = "blur(3px)";
        style["height"] = "120%";
        style["width"] = "120%";
        style["margin"] = "0 -20px";
        style["position"] = "absolute";
        style["z-index"] = "0";
    }
    _clearGameBG() {
        const style = this.game_stylesheet.cssRules[this.game_styleRuleID].style;
        style["background-image"] = "";
        style["background-size"] = "";
        style["background-attachment"] = "";
        style["background-position"] = "";
        style["filter"] = "";
        style["height"] = "";
        style["width"] = "";
        style["margin"] = "";
        style["position"] = "";
        style["z-index"] = "";
    }
    preInit() {
        this._addAnimeImageBGDiv();
        this._addStyles();
    }
    init(dependencies) {
        this.base.injectDependency(this, dependencies, "settings", "settingsModule");
        this.base.injectDependency(this, dependencies, "anilistapi");
        this.base.injectDependency(this, dependencies, "common");
        this.settingsModule.registerSetting({ module: this, name: "Anime as BG", description: "When an answer is revealed, fetch the image for the anime and set it as the quiz background.", id: "animeasbglegacy_animeasbg", value: false, type: this.settingsModule.SETTING_TYPE.BOOL, onchange: undefined });
    }
    async postInit() {
        this.settings = await this.settingsModule.getSettings(this);
        this._initAnimeAsBGListeners();
    }
}
indexbot_base.registerModule("Anime as Background (Legacy)", "animeasbglegacy", "Fetches an image of the anime to use as the quiz background when the answer is revealed.", new IndexbotAnimeAsBGLegacy(), ["settings", "anilistapi", "common"]);
