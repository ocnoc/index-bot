class IndexbotQuizAnilistLink {
    constructor() {
        /**@type {IndexbotAnilistAPI} */
        this.api;
        /**@type {IndexbotSettings} */
        this.settingsModule;
        /**@type {IndexbotCommon} */
        this.common;
        /**@type {IndexbotDatabase} */
        this.database;
        this.base = indexbot_base;
        this.settings;
        this.CACHE_STORAGE_NAME = "anilistlinkcache";
    }
    /**
     * Replaces the anime name on the quiz answer screen with a link to the anime on anilist, titles after the anime.
     * @param {String} link Link to the anilist page
     * @param {String} animeName Name of the anime
     */
    _insertAnilistLink(link, animeName) {
        const DOM_ELEMENT = $("#qpAnimeName");
        let anilistLink = document.createElement("a");
        anilistLink.setAttribute("href", link);
        anilistLink.setAttribute("target", "_blank");
        anilistLink.innerHTML = animeName;
        DOM_ELEMENT.prop("textContent", "");
        DOM_ELEMENT.append(anilistLink);
    }
    /**
     * Interface for getting a link from the cache. Right now just checks a setting and accesses the DB.
     * Probably should get the cache at the beginning as an object and then use puts to update, could cut down on time.
     * @param {String} animeName Name of the anime to get a link for
     */
    async _getCache(animeName) {
        if (!this.settings.cacheurls.getValue()) {
            return undefined;
        }
        return await this.database.get(animeName, this.CACHE_STORAGE_NAME);
    }
    /**
     * Interface for updating the cache with a new link
     * @param {String} animeName Anime name
     * @param {String} url URL for the anilist page
     */
    _putCache(animeName, url) {
        if (!this.settings.cacheurls.getValue()) {
            return;
        }
        let cacheObject = {
            animeName,
            url
        };
        this.database.put(cacheObject, this.CACHE_STORAGE_NAME);
    }
    /**
     * Starts the listener. Listener function runs during every answer results phase.
     */
    _startListener() {
        new Listener("answer results", async (results) => {
            if (!this.settings.insertanilistlink.getValue()) {
                return;
            }
            this.common.debug(this, results);
            let songData = results.songInfo;
            let anime = this.common.getAnimeName(songData.animeNames);
            const ANILIST_LINK = "https://anilist.co/anime/";
            const cachedValue = await this._getCache(anime);
            let animeURL;
            if (cachedValue) {
                this._insertAnilistLink(cachedValue.url, anime);
                return;
            }
            try {
                const id = await this.api.getAnilistAnimeID(anime);
                animeURL = ANILIST_LINK + id;
                this._putCache(anime, animeURL);
            }
            catch (e) {
                this.base.error(this, `Failed to find anime <${anime}> on Anilist, switching to a LMGTFY link. No caching will be done.`);
                animeURL = `https://google.com/search?q=${anime.replace(" ", "+")}`;
            }
            this._insertAnilistLink(animeURL, anime);
        }).bindListener();
    }
    init(dependencies) {
        this.base.injectDependency(this, dependencies, "anilistapi", "api");
        this.base.injectDependency(this, dependencies, "settings", "settingsModule");
        this.base.injectDependency(this, dependencies, "common");
        this.base.injectDependency(this, dependencies, "idbdatabase", "database");
        this.settingsModule.registerSetting({ module: this, name: "Insert Anilist Link", description: "Upon revealing the answer for a quiz, replaces the anime name with a link to the anilist page.", id: "insertanilistlink", value: false, type: this.settingsModule.SETTING_TYPE.BOOL, onchange: null });
        this.settingsModule.registerSetting({ module: this, name: "Cache URLs", description: "Will cache URLs in the database for quicker lookup.", id: "cacheurls", value: false, type: this.settingsModule.SETTING_TYPE.BOOL, onchange: null });
        this.database.registerStorage(this, this.CACHE_STORAGE_NAME, { keyPath: "animeName" });
    }
    async postInit() {
        this.settings = await this.settingsModule.getSettings(this);
        this._startListener();
    }
}
indexbot_base.registerModule("Anilist Quiz Link", "anilistquizlink", "Upon revealing the answer for a quiz, replaces the anime name with a link to the anilist page.", new IndexbotQuizAnilistLink(), ["anilistapi", "settings", "common", "idbdatabase"]);
