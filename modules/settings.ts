
declare var options : any;
function IndexbotSetting({ name, description, id, value, type, onchange, dbmodule, includeOnSettingsPage }: { name; description; id; value; type; onchange; dbmodule; includeOnSettingsPage; }) {
    let newSetting = {name, description, id, value, type, onchange, includeOnSettingsPage, getValue: function() {
        return this.value;
    }, setValue: function(val) {
        this.value = val;
        dbmodule._saveSettings();
        if(onchange) {
            onchange(this)
        }
    }};
    return newSetting;
}

class IndexbotSettings implements IndexbotModule {
    settings: {};
    SETTING_STORAGE_NAME: string;
    database: any;
    common: any;
    base: Indexbot;
    html: any;
    started: boolean;
    SETTINGS_TAB_ID: string;
    SETTINGS_DIV_ID: string;
    SETTINGS_PAGE_NAME: string;
    SETTINGS_ACCORDION_ID: string;
    SETTING_TYPE: { NUMBER: number; STRING: number; BOOL: number; };
    constructor() {
        this.settings = {};
        this.SETTING_STORAGE_NAME = "settings";
        this.database;
        this.common;
        this.base = indexbot_base;
        this.html;
        this.started = false;
        this.SETTINGS_TAB_ID = "indexbotSettingsTab";
        this.SETTINGS_DIV_ID = "indexbotSettingsPage";
        this.SETTINGS_PAGE_NAME = "Indexbot";
        this.SETTINGS_ACCORDION_ID = "indexbotSettingsAccordion";
        this.SETTING_TYPE = {
            NUMBER: 0,
            STRING: 1,
            BOOL: 2
        };
    }
    preInit: () => void;
    /**
     * Registers a new setting with Indexbot Settings. This should be called during init.
     * @param {Module} module Indexbot Module the setting should be registered to
     * @param {String} name Name of the setting
     * @param {String} description Description of the setting
     * @param {String} id An identifier for the setting
     * @param {*} value Default value for the setting
     * @param {*} type The type for the setting value
     * @param {function} onchange Callback function. Called when the value is changed, the setting object is passed in.
     * @param {bool} includeOnSettingsPage If the settings module should include the setting when building the main settings page
     */
    registerSetting({ module, name, description, id, value, type, onchange, includeOnSettingsPage = true }: { module; name; description; id; value; type; onchange?: (arg0: Object) => void; includeOnSettingsPage?: boolean; }) {
        let moduleID = module.descriptor().id;
        if (!this.settings[moduleID]) {
            this.settings[moduleID] = {};
        }
        let newSetting = IndexbotSetting({ name, description, id, value, type, onchange, dbmodule: this, includeOnSettingsPage });
        this.settings[moduleID][id] = newSetting;
    }
    /**
     * Gets the settings associated with a specific module.
     * @param {Module} module Module to get settings for
     * @returns {Object} an object where every property links to a setting object for that module
     */
    async getSettings(module) {
        if (!this.started) {
            await this._startSettings();
        }
        let moduleSettings = this.settings[module.descriptor().id];
        return moduleSettings;
    }
    _alertSetValue(setting, value) {
        setting.setValue(value);
        this.common.alert("Setting saved!");
    }
    _addSettingsTab(title, id, divID) {
        const $TAB_CONTAINER = $("#settingModal .tabContainer");
        const ONCLICK = `options.selectTab('${divID}', this)`;
        let newTab = document.createElement("div");
        let newTabTitle = document.createElement("h5");

        newTab.id = id;
        newTab.setAttribute("onclick", ONCLICK);
        newTab.classList.add("tab", "leftRightButtonTop", "clickAble");

        newTabTitle.textContent = title;

        newTab.appendChild(newTabTitle);
        $TAB_CONTAINER.append(newTab);

        //AMQ Global Options variable
        options.$SETTING_TABS = $("#settingModal .tab");

        return newTab;
    }
    _addSettingsDiv(id) {
        const $PAGE_CONTAINER = $("#settingModal .modal-body");
        let newDiv = document.createElement("div");

        newDiv.classList.add("settingContentContainer", "hide", "indexbot-settings");
        newDiv.id = id;

        $PAGE_CONTAINER.append(newDiv);

        //AMQ Global Options Variable
        options.$SETTING_CONTAINERS = $(".settingContentContainer");

        return newDiv;
    }
    addSettingsPage(title, divID, tabID) {
        let div = this._addSettingsDiv(divID);
        let tab = this._addSettingsTab(title, tabID, divID);
        return { div, tab };
    }
    _getInputType(settingType) {
        return settingType <= this.SETTING_TYPE.STRING ? "text" : "checkbox";
    }
    _addSetting(table, setting) {
        let row = document.createElement("tr");
        let data1 = document.createElement("td");
        let input = document.createElement("input");
        let label = document.createElement("td");
        let type = this._getInputType(setting.type);
        let description = document.createElement("td");
        const TEXT = "text";
        const CHECK = "checkbox";

        input.setAttribute("type", type);
        label.textContent = setting.name;
        description.textContent = setting.description;
        row.appendChild(label);
        row.appendChild(description);
        row.appendChild(data1);
        data1.appendChild(input);

        if (type == TEXT) {
            let updateButton = document.createElement("button");
            updateButton.classList.add("btn", "btn-primary");
            input.classList.add("form-control", "form-control-inline");
            updateButton.textContent = "Update";
            updateButton.onclick = () => {
                this._alertSetValue(setting, input.value);
            };
            data1.append(updateButton);
            input.value = setting.getValue();
        }
        else {
            input.checked = setting.getValue();
            input.onclick = () => {
                this._alertSetValue(setting, input.checked);
            };
        }

        table.append(row);
        return input;
    }
    _buildSettingsPage() {
        let { div: settingsDiv, tab: settingsTab } = this.addSettingsPage(this.SETTINGS_PAGE_NAME, this.SETTINGS_DIV_ID, this.SETTINGS_TAB_ID);
        let settingGroup = this.html.createAccordionContainer({ id: this.SETTINGS_ACCORDION_ID, parent: settingsDiv });
        Object.keys(this.settings).forEach(moduleID => {
            let moduleDescriptor = this.base.getModuleDescriptor(moduleID);
            let elementID = `indexbotSettingsCategory${moduleDescriptor.id}`;
            let headerID = `indexbotSettingsCategoryHeader${moduleDescriptor.id}`;
            let bodyID = `indexbotSettingsCategoryBody${moduleDescriptor.id}`;
            let tableID = `indexbotSettingsTable${moduleDescriptor.id}`;
            this.html.createAccordionElement({ title: moduleDescriptor.name, elementID: elementID, headerID: headerID, bodyID: bodyID, parentID: this.SETTINGS_ACCORDION_ID });
            let $PANEL_BODY = $(`#${bodyID}`);
            let settingsTable = this.html.createTable({
                id: tableID
            });
            settingsTable.classList.add("table-bordered", "indexbotSettingsTable");
            $PANEL_BODY.append(settingsTable);
            let tableBody = $(`#${tableID} tbody`);
            Object.values(this.settings[moduleID]).filter(setting => (<any>setting).includeOnSettingsPage).forEach(setting => {
                this._addSetting(tableBody, setting);
            });
        });
        // Init scrollbar
        (<any>$(`#${this.SETTINGS_DIV_ID}`)).perfectScrollbar();

    }
    _saveSettings() {
        return new Promise<void>((resolve, reject) => {
            Object.keys(this.settings).forEach(key => {
                let moduleID = key;
                let settings = Object.values(this.settings[key]).map(function ({ id, value }) {
                    return { id, value };
                });
                let storedSettings = {
                    moduleID: moduleID,
                    settings: {}
                };
                settings.forEach(function ({ id, value }) {
                    storedSettings.settings[id] = value;
                });
                this.database.put(storedSettings, this.SETTING_STORAGE_NAME);
            });
            resolve();
        });
    }
    async _loadSetings() {
        await Promise.all(Object.keys(this.settings).map(async (moduleID) => {
            let settingObject = await this.database.get(moduleID, this.SETTING_STORAGE_NAME);
            if (settingObject) {
                Object.keys(settingObject.settings).forEach(settingID => {
                    if (this.settings[moduleID]) {
                        this.settings[moduleID][settingID].value = settingObject.settings[settingID];
                    }
                });
            }
            return;
        }));
        return;
    }
    async _startSettings() {
        if (this.started) {
            return;
        }
        await this._loadSetings();
        await this._saveSettings();
        this.started = true;
    }
    init(dependencies: ModuleDescriptor[]) {
        this.base.injectDependency(this, dependencies, "idbdatabase", "database");
        this.base.injectDependency(this, dependencies, "common");
        this.base.injectDependency(this, dependencies, "html");
        this.database.registerStorage(this, this.SETTING_STORAGE_NAME, { keyPath: "moduleID" }, []);
    }
    async postInit() {
        await this._startSettings();
        this.common.includeExternalCSS(this, "https://gl.githack.com/ocnoc/index-bot/-/raw/master/modules/css/settings.css");
        this._buildSettingsPage();
    }
}

indexbot_base.registerModule("Indexbot Settings", "settings", "Allows modules to store, modify and access settings.", new IndexbotSettings(), ["idbdatabase", "common", "html"]);