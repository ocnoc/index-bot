/**
 * Port of round report from the last indexbot version
 */
class IndexbotRoundReport {
    constructor() {
        /**@type {IndexbotHTML} */
        this.html;
        /**@type {IndexbotCommon} */
        this.common;
        this.base = indexbot_base;
        this.roundData;

        this.ROUND_REPORT_MODAL_ID = "indexbotrrmodal";
        this.ROUND_REPORT_MODAL_BODY_ID = "indexbotrrmodalbody";
        this.ROUND_REPORT_MODAL_HEADER_ID = "indexbotrrmodalheader";
        this.ROUND_REPORT_BUTTON_ID = "roundreportbutton";

        this.ROUND_REPORT_SUBTITLE_ID = "rrheadersubtitle";
        this.ROUND_REPORT_SONG_COUNT_ID = "rrheadersongcount";
        this.ROUND_REPORT_SCORE_ID = "rrheaderscore";
        this.ROUND_REPORT_PLACEMENT_ID = "rrheaderplacement";

        this.ROUND_REPORT_TABLE_BODY_ID = "rrtablebody";
        this.ROUND_REPORT_TABLE_HEADER_ID = "rrtableheader";
        this.ROUND_REPORT_TABLE_ID = "rrtable";
    }
    _initRoundReportTable() {
        let rrbody = $(`#${this.ROUND_REPORT_MODAL_BODY_ID}`);
        let table = this.html.createTable({
            parent: rrbody,
            id: this.ROUND_REPORT_TABLE_ID,
            headID: this.ROUND_REPORT_TABLE_HEADER_ID,
            bodyID: this.ROUND_REPORT_TABLE_BODY_ID
        });
        return table;
    }
    _initRoundReportBody() {
        let table = this._initRoundReportTable();
        let header = $(`#${this.ROUND_REPORT_TABLE_HEADER_ID}`);
        let body = $(`#${this.ROUND_REPORT_TABLE_BODY_ID}`);
        let rrBody = $(`#${this.ROUND_REPORT_MODAL_BODY_ID}`);

        header.replaceWith(
            `  <thead>
    <tr>
        <th scope="col">Anime</th>
        <th scope="col">Guess</th>
        <th scope="col">Correct</th>
    </tr>
    </thead>`);

        rrBody.addClass("roundReportScrolling");
        rrBody.perfectScrollbar();
    }
    _initRoundReportHeader() {
        let rrHeader = $("#" + this.ROUND_REPORT_MODAL_HEADER_ID);
        let rrSubtitle = document.createElement("h3");
        rrHeader.append(rrSubtitle);
        rrSubtitle.outerHTML = `<h3 class="modal-title" id="${this.ROUND_REPORT_SUBTITLE_ID}">Songs: 
    <span id="${this.ROUND_REPORT_SONG_COUNT_ID}">0</span>
     | Score:
    <span id="${this.ROUND_REPORT_SCORE_ID}">0</span>
     | Position:
    <span id="${this.ROUND_REPORT_PLACEMENT_ID}">0</span>
  </h3>`;
    }
    showRoundReport() {
        $(`#${this.ROUND_REPORT_MODAL_ID}`).modal();
    }
    clearRoundReport() {
        $(`#${this.ROUND_REPORT_TABLE_BODY_ID} > tr`).remove();
    }
    _initRoundReportModal() {
    }
    _initRoundReportListeners() {
    }
    generateRoundReport(data) {
    }
    _start() {
        let module = this;
        const QP_ICON = "fa-book";
        const MODAL_TITLE = "Round Report";
        const TOOLTIP = "Show a list of songs that have occurred this game.";
        const callback = () => {
            module._showAndUpdateRoundReport();
        };

        this.html.createModalWindow({ title: MODAL_TITLE, windowID: this.ROUND_REPORT_MODAL_ID, headerID: this.ROUND_REPORT_MODAL_HEADER_ID, bodyID: this.ROUND_REPORT_MODAL_BODY_ID });
        this.html.addRoundOptionButton({ faIconClass: QP_ICON, tooltip: TOOLTIP, onclick: callback, id: this.ROUND_REPORT_BUTTON_ID });
        this._initRoundReportModal();
        this._initRoundReportBody();
        this._initRoundReportHeader();
        this._initRoundReportListeners();
    }
    init(dependencies) {
        this.base.injectDependency(this, dependencies, "html");
        this.base.injectDependency(this, dependencies, "common");
    }
    postInit() {
        this._start();
    }
}












indexbot_base.registerModule("Round Report", "roundreport", "Allows users to access a round report of the songs seen in the current or previous round.", new IndexbotRoundReport(), ["html", "common"]);