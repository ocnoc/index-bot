class IndexbotDatabase {
    constructor() {
        this.BACKEND_DB_VERSION = 1;
        this.BACKEND_DATABASE;
        this.MAIN_DATABASE;
        this.MAIN_DB_NAME = "Indexbot";
        this.BACKEND_DB_NAME = "IndexbotIDBDatabase";
        this.MAIN_DB_SCHEMA_NAME = "schemas";
        this.STORAGE_SCHEMA = {
            dbName: this.MAIN_DB_NAME,
            version: 1,
            stores: {}
        };
        this.TRANSACTION_TYPE = {
            READWRITE: "readwrite",
            READ: "readonly",
        };
        this.POST_INIT_ERROR = "You must run this function during or after PostInit.";

        this.base = indexbot_base;
        this.storesToCreate = [];
        this.changeNeeded = false;
    }
    /**
     *
     * @param {Object} module The module adding the storage
     * @param {String} storageName Name of the store
     * @param {Object} options An IndexedDB options object
     * @param {Array} indexes An array of indexes to make. Can be empty. Indexes should be {name, keyPath, options} where parameters are from IndexedDB index parameters
     */
    registerStorage(module, storageName, options, indexes = []) {
        let moduleID = module.descriptor().id;
        if (this.base.state != this.base.states.INIT) {
            this.base.error(this, `Error registering storage for module ${module.descriptor().name}. You must register storage during the init phase!`);
            return;
        }
        let existing_stores = Object.keys(this.STORAGE_SCHEMA.stores);
        if (existing_stores.includes(storageName)) {
            if (this.STORAGE_SCHEMA.stores[storageName].owner != moduleID) {
                this.base.error(this, `Error registering storage for module ${module.descriptor().name}. A store with name [${storageName}] already exists.`);
            }
            return;
        }
        let owner = module.descriptor().id;
        this.STORAGE_SCHEMA.stores[storageName] = { storageName, options, indexes, owner };
        this.storesToCreate.push({ storageName, options, indexes });
        if (!this.changeNeeded) {
            this.changeNeeded = true;
            this.STORAGE_SCHEMA.version += 1;
        }

    }
    /**
     * Begins the connection to the main database.
     */
    beginMainDBConnection() {
        return new Promise((resolve, reject) => {
            if (this.MAIN_DATABASE) {
                resolve();
                return;
            }
            let request = window.indexedDB.open(this.MAIN_DB_NAME, this.STORAGE_SCHEMA.version);

            request.onsuccess = (event) => {
                this.MAIN_DATABASE = event.target.result;
                resolve();
            };

            request.onerror = (event) => {
                this.base.error(this, "Error opening connection to database. Make sure IndexedDB is allowed.");
                console.log(request.error);
                reject();
            };

            request.onupgradeneeded = (event) => {
                const db = event.target.result;

                this.storesToCreate.forEach((store) => {
                    let storage = db.createObjectStore(store.storageName, store.options);
                    store.indexes.forEach((index) => {
                        storage.createIndex(index.name, index.keyPath, index.options);
                    });
                });
            };
        });
    }
    /**
     * Protector function. Checks to see if the current state is before postinit, and if the database has not been loaded, loads it.
     */
    async postInitCheck() {
        if (this.base.state < this.base.states.POSTINIT) {
            this.base.error(this, this.POST_INIT_ERROR);
            return false;
        }
        if (!this.MAIN_DATABASE) {
            await this.beginMainDBConnection();
        }
        return true;
    }
    /**
     * Looks up an object in a given store by a given index key
     * @param {String} key The keyvalue of the index
     * @param {String} store The storage to look in
     * @param {String} index The attribute to index by
     */
    async getIndex(key, store, index) {
        let check = await this.postInitCheck();
        if (!check) {
            throw new Error(this.POST_INIT_ERROR);
        }
        return new Promise((resolve, reject) => {
            let transaction = this.MAIN_DATABASE.transaction(store, this.TRANSACTION_TYPE.READ);
            let request = transaction.objectStore.index(index).get(key);
            request.onsuccess = (event) => {
                resolve(event.target.result);
            };
            request.onerror = (event) => {
                reject(request.error);
            };
        });
    }
    /**
     * Gets an object with a given keyvalue from the target storage.
     * @param {String} key key value of the object to get
     * @param {String} store The objectstore to get the target from
     */
    async get(key, store) {
        let check = await this.postInitCheck();
        if (!check) {
            throw new Error(this.POST_INIT_ERROR);
        }
        return new Promise((resolve, reject) => {
            let transaction = this.MAIN_DATABASE.transaction(store, this.TRANSACTION_TYPE.READ);
            let request = transaction.objectStore(store).get(key);
            request.onsuccess = (event) => {
                resolve(event.target.result);
            };
            request.onerror = (event) => {
                reject(request.error);
            };

        });
    }
    /**
     * Stores an object in an ObjectStore. Will error/reject if the object with the store's keyvalue already exists.
     * @param {Object} object The object to store
     * @param {String} store The target storage
     */
    async add(object, store) {
        let check = await this.postInitCheck();
        if (!check) {
            throw new Error(this.POST_INIT_ERROR);
        }
        return new Promise((resolve, reject) => {
            let transaction = this.MAIN_DATABASE.transaction(store, this.TRANSACTION_TYPE.READWRITE);
            let request = transaction.objectStore(store).add(object);
            request.onsuccess = (event) => {
                resolve(event.target.result);
            };
            request.onerror = (event) => {
                reject(request.error);
            };
        });
    }
    /**
     * Store an object in a given objectsotre, overriding any existing object with the same keyvalue
     * @param {Object} object Object to be stored
     * @param {String} store Objectstore to store the object in
     */
    put(object, store) {
        return new Promise(async (resolve, reject) => {
            let check = await this.postInitCheck();
            if (!check) {
                reject();
                return;
            }
            let transaction = this.MAIN_DATABASE.transaction(store, this.TRANSACTION_TYPE.READWRITE);
            let request = transaction.objectStore(store).put(object);
            request.onsuccess = (event) => {
                resolve(event.target.result);
            };
            request.onerror = (event) => {
                reject(request.error);
            };
        });
    }
    /**
     * Backend function. Loads the schema for the main database form the backend database.
     */
    _loadMainSchema() {
        return new Promise((resolve, reject) => {
            const database = this.BACKEND_DATABASE;

            let transaction = database.transaction(this.MAIN_DB_SCHEMA_NAME, this.TRANSACTION_TYPE.READWRITE);
            const store = transaction.objectStore(this.MAIN_DB_SCHEMA_NAME);
            let request = store.get(this.MAIN_DB_NAME);

            request.onsuccess = (event) => {
                this.STORAGE_SCHEMA = event.target.result;
                resolve();
            };
        });
    }
    /**
     * Backend function. Stores the schema for the main database in the backend database. Should be called in postinit, after all stores are registered.
     */
    _storeMainSchema() {
        const database = this.BACKEND_DATABASE;

        let transaction = database.transaction(this.MAIN_DB_SCHEMA_NAME, this.TRANSACTION_TYPE.READWRITE);
        const store = transaction.objectStore(this.MAIN_DB_SCHEMA_NAME);
        store.put(this.STORAGE_SCHEMA);
    }
    /**
     * Backend function. Connects to the backend database for metadata about the main database.
     * @param {int} version Version of the backend database
     */
    _beginBackendConnection(version) {
        return new Promise((resolve, reject) => {
            let request = window.indexedDB.open(this.BACKEND_DB_NAME, version);
            request.onerror = (event) => {
                this.base.error(this, "Error opening connection to backend database. Make sure IndexedDB is allowed.");
                reject();
                return;
            };

            request.onsuccess = (event) => {
                this.BACKEND_DATABASE = event.target.result;
                resolve();
            };

            request.onupgradeneeded = (event) => {
                const db = event.target.result;

                if (event.oldVersion < 1) {
                    let mainSchema = db.createObjectStore(this.MAIN_DB_SCHEMA_NAME, { keyPath: "dbName" });
                    mainSchema.transaction.oncomplete = (event) => {
                        let store = db.transaction(this.MAIN_DB_SCHEMA_NAME, this.TRANSACTION_TYPE.READWRITE).objectStore(this.MAIN_DB_SCHEMA_NAME);
                        store.add(this.STORAGE_SCHEMA);
                    };
                }
            };
        });
    }
    async preInit() {
        await this._beginBackendConnection(this.BACKEND_DB_VERSION);
        await this._loadMainSchema();
    }
    init() {
    }
    /**
     * Everyone has registered storages. the MainDB connection may be started earlier if another moduel calls an accessor function.
     */
    async postInit() {
        await this.beginMainDBConnection();
        this._storeMainSchema();
    }
}














indexbot_base.registerModule("Indexbot IndexedDB Database", "idbdatabase", "Provides support for modules to store data in the borwser's IndexedDB", new IndexbotDatabase());