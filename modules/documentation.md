# Loading Modules
Load order should not matter aside from *base* being included first somehow.
## Greasemonkey Require
Greasemonkey comes with a built in @require annotation that will pull in the given javascript file. This is the default way the Indexbot file will pull in modules (including base). This is the easiest way to add new modules. However, there's a slight catch. Greasemonkey downloads the required files on script save and stores them. It will only redownload when the script is saved again with changes. This means requiring modules from a repository won't automatically pull in updates until you resave the base script.
## Common Include
Indexbot Common comes with an include function that puts the included script directly into the header of the page. However, this is meant for modules that need to pull in external dependencies. This may also result in some ugly looking code on the greasemonkey file. This should not be used for loading modules, but is an option.
## Module Controller
The Module Controller is an Indexbot Module that allows users to include modules using a user interface that can pull from both the official indexbot repository and user added repositories.
# Developing Modules
New modules can be developed in their own javascript files and included using one of the methods above.
## registerModule
The registerModule function in Indexbot Base has the following identifier.
```javascript
registerModule(name, id, description, module, requiredModules=null)
```
This function must be called to load a module once Indexbot starts. *module* should be a copy of your module object. *id* should be a unique string identifier. If another module is already using that identifier on that start, Indexbot Base will reject your registration and tell you. *requiredModules* can be an array of module identifiers, which will be given to your function in *init*.
## Initialization Phases
Modules can have three functions that will be called in phases during base startup.
### preInit
First initialization called. Assume your module is the only one loaded aside from base. Good time to setup any backend connections or prepare your functions to be called.
### init
Second phase. Dependencies are passed into this function as an array of descriptors. These can be stored into your own module and used. Some modules may not be fully ready for intermodule interaction. This is usually where many modules that use persistent storage will allow registration of storage usage, such as database object stores, or settings.
### postInit
Final phase. All modules should be ready for full use. For modules where load order may matter, such as database needing to initiate a connection before getting data, functions should have protection applied to ensure load order won't matter.