(function () {
    const REQUIRED_MODULES = ["common", "html"]; // These are necessary for the controller to function for now.
    const DB_VERSION = 1;
    const SCHEMA_VERSION = 1;
    const BASE_REPOSITORY = "https://gl.githack.com/ocnoc/index-bot/-/raw/master/modules/module_repo.json";
    const BASE_REPO_ID = "official";
    const STORE_NAME = "schema";

    class IndexbotModuleController implements IndexbotModule {
        html: IIndexbotHTML;
        common: IndexbotCommon;
        base: Indexbot;
        repositoryData: {};
        schema: { version: number; repositories: string[]; loadedModules: { id: string; repoidentifier: string; repository: string; }[]; };
        db: IDBDatabase;
        MODAL_ID: string;
        MODAL_HEADER_ID: string;
        MODAL_BODY_ID: string;
        OPTION_BUTTON_ID: string;
        REPO_COUNT_ID: string;
        ACCORDION_CONTAINER_ID: string;
        constructor() {
            /**@type {IndexbotHTML} */
            this.html;
            /**@type {IndexbotCommon} */
            this.common;
            /**@type {Indexbot} */
            this.base = indexbot_base;

            this.repositoryData = {};

            this.schema = {
                version: SCHEMA_VERSION,
                repositories: [BASE_REPOSITORY],
                loadedModules: [
                    {
                        id: "html",
                        repoidentifier: "html",
                        repository: BASE_REPO_ID
                    },
                    {
                        id: "common",
                        repoidentifier: "common",
                        repository: BASE_REPO_ID
                    }
                ]
            };

            /**@type {IDBDatabase} */
            this.db;

            this.MODAL_ID = "modulecontrollerwindow";
            this.MODAL_HEADER_ID = "modulecontrollerheader";
            this.MODAL_BODY_ID = "modulecontrollerbody";
            this.OPTION_BUTTON_ID = "modulecontrolleroption";
            this.REPO_COUNT_ID = "modulecontrollerrepocounter";
            this.ACCORDION_CONTAINER_ID = "modulecontrolleraccordioncontainer";

            this.base.registerAwaitedHook(this, indexbot_base, "start", this._start); // Not an amazing way to do this
        }
        preInit: () => void;
        /**
             * Starts everything for the controller. Should be called in a hook to bases' start function.
             */
        async _start() {
            let schema;
            await this._startDBConnection();
            schema = await this._loadSchema();
            this.schema = schema;
            await this._loadSchemaRepositoryData();
            await this._startModulesLoad();
        }
        _startDBConnection() {
            return new Promise<void>((resolve, reject) => {
                let request = window.indexedDB.open("IndexbotModuleControllerDB", DB_VERSION);
                const KEYPATH = "version";

                request.onsuccess = event => {
                    this.db = request.result;
                    resolve();
                };

                request.onerror = _event => {
                    reject("Failed to initialize IndexedDB connection");
                };

                request.onupgradeneeded = event => {
                    /**@type {IDBDatabase} */
                    let db = request.result;

                    if (event.oldVersion < 1) {
                        const storeRequest = db.createObjectStore(STORE_NAME, { keyPath: KEYPATH });

                        storeRequest.transaction.oncomplete = event => {
                            let store = db.transaction(STORE_NAME, "readwrite").objectStore(STORE_NAME);
                            store.put(this.schema);
                        };
                    }
                };
            });
        }
        _saveSchema() {
            return new Promise<void>((resolve, reject) => {
                const MODE = "readwrite";
                let store = this.db.transaction(STORE_NAME, MODE).objectStore(STORE_NAME);
                let request = store.put(this.schema);

                request.onsuccess = _event => {
                    resolve();
                };

                request.onerror = _event => {
                    reject(request.error);
                };

            });
        }
        _loadSchema() {
            return new Promise((resolve, reject) => {
                const MODE = "readwrite";
                let store = this.db.transaction(STORE_NAME, MODE).objectStore(STORE_NAME);
                let request = store.get(SCHEMA_VERSION);

                request.onsuccess = _event => {
                    resolve(request.result);
                };

                request.onerror = _event => {
                    reject(request.error);
                };
            });
        }
        async _loadSchemaRepositoryData() {
            await Promise.all(this.schema.repositories.map(async (repoURL) => {
                try {
                    let repoData = await this._loadRepositoryData(repoURL);
                    if (repoData) {
                        this.repositoryData[repoData.id] = repoData;
                    }
                }
                catch (e) {
                    this.base.error(this, `Failed to load repository data at ${repoURL}`);
                }
            }));
        }
        async _loadRepositoryData(repositoryURL) {
            let request = await fetch(repositoryURL, { cache: "no-store" });
            if (!request.ok) {
                throw new Error("Failed to fetch repository URL");
            }
            let data = await request.json();
            return data;
        }
        /**
             * Internal function pretty much copy pasted from common, necessary since we dont have access to common when we need this.
             * @param {String} url URL of a module
             */
        _includeModule(url) {
            return new Promise<void>((resolve, reject) => {
                let script = document.createElement("script");
                script.src = url + "?ts=" + new Date().getTime();
                script.async = false;
                script.onload = function () {
                    resolve();
                };
                document.head.appendChild(script);
                this.base.log(this, `Loading a module from ${url}`);
            });
        }
        async _startModulesLoad() {
            await Promise.all(this.schema.loadedModules.map(loadedModule => {
                let repositoryID = loadedModule.repository;
                let moduleID = loadedModule.id;
                let moduleRepoID = loadedModule.repoidentifier;
                let repository = this.repositoryData[repositoryID];
                let url;
                repository.modules.forEach(repoModule => {
                    if (repoModule.repoidentifier == moduleRepoID) {
                        url = repoModule.url;
                    }
                });
                return this._includeModule(url);
            }));
        }
        _removeModulesFromRepo(repositoryID) {
            this.schema.loadedModules = this.schema.loadedModules.filter(module => module.repository != repositoryID);
            this.common.alert(`Unloaded modules linked to repository id [${repositoryID}]`);
        }
        _deleteRepository(repositoryID) {
            let repoData = this.repositoryData[repositoryID];
            let repoURL = repoData.repoURL;
            this.schema.repositories = this.schema.repositories.filter(repo => repo != repoURL);
            this._removeModulesFromRepo(repositoryID);
            this.repositoryData[repositoryID] = undefined;
            this.common.alert(`Removed repository [${repoData.name}]`);
            this._saveSchema();
            this._updateModal();
        }
        async _addRepository(url) {
            if (this.schema.repositories.includes(url)) {
                this.common.alert(`Repository at ${url} is already loaded.`, "warning");
                return false;
            }
            try {
                await this._loadRepositoryData(url);
            }
            catch (e) {
                this.common.alert("An error occurred while adding the repository.", "error");
                return;
            }
            this.schema.repositories.push(url);
            this._saveSchema();
            this._updateModal();
        }
        _addModule(id, repoidentifier, repository) {
            this.schema.loadedModules.push({ id, repoidentifier, repository });
            this._saveSchema();
        }
        _removeModule(id, repoidentifier, repository) {
            this.schema.loadedModules = this.schema.loadedModules.filter(module => module != { id, repoidentifier, repository });
            this._saveSchema();
        }
        _updateModal() {
            $(`#${this.REPO_COUNT_ID}`).text(this.schema.repositories.length);
        }
        _isModuleLoaded(repoModule, repoID) {
            let res = this.schema.loadedModules.filter(module => module.repository == repoID && module.repoidentifier == repoModule.repoidentifier).length > 0;
            return res;
        }
        _updateModuleState(repoModule, repositoryID, checked) {
            if (checked) {
                this._addModule(repoModule.id, repoModule.repoidentifier, repositoryID);
            }
            else {
                this._removeModule(repoModule.id, repoModule.repoidentifier, repositoryID);
            }
            this.common.alert("Module status updated!");
        }
        _createModuleTable(repoData, bodyID) {
            let tableID = bodyID + "t";
            let header;
            let body;

            let table = this.html.createTable({
                id: tableID,
                parent: $(`#${bodyID}`)
            });
            header = $(`#${tableID} > thead`);
            body = $(`#${tableID} > tbody`);

            header.html(`
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Description</th>
            <th scope="col">Requires</th>
            <th scope="col">Optional</th>
            <th scope="col">Active</th>
        </tr>
    `);

            repoData.modules.forEach(module => {
                let row = document.createElement("tr");
                let inputID = tableID + repoData.id + module.repoidentifier;
                let self = this;
                let onclick = function () {
                    self._updateModuleState(module, repoData.id, $(`#${inputID}`).prop("checked"));
                };

                body.append(row);

                row.outerHTML = `<tr>
            <td>${module.name}</td>
            <td>${module.description}</td>
            <td>${module.requirements.toString()}</td>
            <td>${module.optional.toString()}</td>
            <td>
                <input type="checkbox" class="form-check" id=${inputID}>
            </td>
        </tr>`;

                let input = $(`#${inputID}`);
                //checked=${this._isModuleLoaded(module, repoData.id)} onclick=${onclick}
                input.prop("checked", this._isModuleLoaded(module, repoData.id));
                input.click(onclick);
            });

        }
        _createBody() {
            let modalBody = $(`#${this.MODAL_BODY_ID}`);
            let accordionContainer = this.html.createAccordionContainer({ id: this.ACCORDION_CONTAINER_ID, parent: modalBody });
            Object.keys(this.repositoryData).forEach(repoid => {
                let repoData = this.repositoryData[repoid];
                const ELEMENT_ID = `indexbotmda${repoid}`;
                const BODY_ID = `indexbotmdab${repoid}`;
                const HEADER_ID = `indexbotmdah${repoid}`;

                this.html.createAccordionElement({ title: repoData.repoName, elementID: ELEMENT_ID, headerID: HEADER_ID, bodyID: BODY_ID, parentID: this.ACCORDION_CONTAINER_ID });
                $(`#${HEADER_ID} button`).addClass("btn-lg");

                this._createModuleTable(repoData, BODY_ID);

            });
        }
        _createHeader() {
            let subHeader = document.createElement("h3");
            let repoLine = document.createElement("div");
            let repositoryTextBox = document.createElement("input");
            let repositoryAddButton = document.createElement("button");
            let header = $(`#${this.MODAL_HEADER_ID}`);
            let module = this;
            let addButtonCallback = function () {
                module._addRepository(repositoryTextBox.value);
                repositoryTextBox.value = "";
            };

            repoLine.append(repositoryTextBox, repositoryAddButton);
            header.append(subHeader);
            header.append(repoLine);

            subHeader.outerHTML = `<h3 class="modal-title">Repository Count: <span id="${this.REPO_COUNT_ID}">${this.schema.repositories.length}</span></h3>`;

            repositoryTextBox.classList.add("form-control");
            repositoryTextBox.setAttribute("type", "text");
            repositoryTextBox.placeholder = "Repository URL";

            repositoryAddButton.classList.add("btn", "btn-primary");
            repositoryAddButton.onclick = addButtonCallback;
            repositoryAddButton.textContent = "Add Repository";

            repoLine.classList.add("text-center");
        }
        _createModuleControllerWindow() {
            this.html.createModalWindow({ title: "Module Controller", windowID: this.MODAL_ID, headerID: this.MODAL_HEADER_ID, bodyID: this.MODAL_BODY_ID });
            $(`#${this.MODAL_ID} >`).addClass("module-controller-modal");
            this._createHeader();
            this._createBody();
        }
        _createOptionButton() {
            let module = this;
            let callback = function () {
                module.displayModuleController();
            };
            this.html.addOptionButton({ title: "Module Controller", buttonID: this.OPTION_BUTTON_ID, onclick: callback });
        }
        displayModuleController() {
            (<any>$(`#${this.MODAL_ID}`)).modal();
        }
        init(dependencies) {
            this.base.injectDependency(this, dependencies, "html");
            this.base.injectDependency(this, dependencies, "common");
        }
        postInit() {
            this._createModuleControllerWindow();
            this._createOptionButton();
            this.common.includeExternalCSS(this, "https://gl.githack.com/ocnoc/index-bot/-/raw/master/modules/css/modulecontroller.css");
        }
    }


























    indexbot_base.registerModule("Indexbot Module Controller", "modulecontroller", "Allows GUI control of what modules get loaded.", new IndexbotModuleController(), REQUIRED_MODULES);
}
)()