declare const iziToast : any;;
declare const Swal : any;
/**
 * A set of common functions to use across Indexbot modules.
 */
class IndexbotCommon implements IndexbotModule {
    loaded_scripts: any[];
    loaded_css: any[];
    settingsModule: IndexbotSettings;
    settings: any;
    base: Indexbot;
    constructor() {
        this.loaded_scripts = [];
        this.loaded_css = [];
        this.settingsModule;
        this.settings;
        this.base = indexbot_base;
    }
    /**
     * Includes a foreign script into this one. Similar to greasemonkey require.
     * @param {String} script_url URL to the script to include
     * @param {Object} caller Module including the script
     */
    include(caller, script_url) {
        if (this.loaded_scripts.includes(script_url)) {
            return;
        }
        let script = document.createElement("script");
        script.src = script_url;
        document.head.appendChild(script);
        this.base.log(this, `Module [${caller.descriptor().name}] attempted to load a script from ${script_url}`);
        this.loaded_scripts.push(script_url);
    }
    /**
     * Includes a foreign css file into the webpage.
     * @param {Object} caller Module that includes the CSS
     * @param {String} css_url URL to the external css file
     */
    includeExternalCSS(caller, css_url) {
        let css = document.createElement("link");
        css.href = css_url;
        css.rel = "stylesheet";
        css.type = "text/css";
        document.head.append(css);
        this.base.log(this, `Module [${caller.descriptor().name}] attempted to load a CSS file from ${css_url}`);
        this.loaded_css.push(css_url);
    }
    /**
     * Sends a message to the user. Uses izitoast for messages.
     * @param {String} message The message to send
     * @param {String} messageType The type of message to send
     */
    alert(message, messageType = "info") {
        let mapping = {
            info: iziToast.info,
            success: iziToast.success,
            error: iziToast.error,
            warning: iziToast.warning
        };
        let notificationMethod = mapping[messageType];
        let settings = {
            title: "Indexbot",
            message: message,
            position: "topLeft"
        };
        notificationMethod.call(iziToast, settings);
    }
    /**
     * Displays a full screen message the user cannot escape from without refreshing. Useful for blocking further inputs until some promise has returned.
     * @param {String} message The message to display
     * @param {String} icon The icon to use
     */
    loadingAlert(message, icon?) {
        Swal.fire({
            title: "Indexbot",
            text: message,
            allowOutsideClick: false,
            allowEscapeKey: false,
            allowEnterKey: false,
            showConfirmButton: false,
            type: icon,
        });
    }
    /**
     * Wrapper for closing any full screen alert up.
     */
    closeLoadingAlert() {
        Swal.close();
    }
    /**
     * A promise that resolves after ms time, useful for awaiting.
     * @param {int} ms Time to sleep
     */
    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    /**
     * Gives a random number between min-max, including max
     * @param {int} min minimum
     * @param {int} inclusiveMax inclusive maximum
     */
    randomInt(min, inclusiveMax) {
        inclusiveMax++;
        inclusiveMax = inclusiveMax - min;
        return Math.floor((Math.random() * Math.floor(inclusiveMax)) + min);
    }
    /**
     * Hacky way to remove accents and other weird marks from a string. normalize changes accented characters to be represented by
     * char + accent, then the replace strips accents/diacritics/etc.
     * @param {String} msg string to remove accents from
     */
    stripAccents(msg) {
        return msg.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    }
    /**
     * Gets a correct anime name from a names object based on user settings on AMQ
     * @param {*} animeNames Object containing remoji and english properties
     */
    getAnimeName(animeNames) {
        return options.useRomajiNames ? animeNames.romaji : animeNames.english;
    }
    /**
     * Will output the given object to console if user has debug setting enabled.
     * Should be used if your intended output may be spammy or not useful for end users.
     * @param {Module} module Indexbot module making the debug call
     * @param {*} msg What to log
     */
    debug(module, msg) {
        if (!this.settings || !this.settings.debug.getValue()) {
            return;
        }
        this.base.log(module, `DEBUG OUTPUT`);
        console.log(msg);
    }
    /**
     * Returns an object with the quiz player data and payload player data for a given player, quiz player object and payload player list
     * @param {String} name Name of the player to collect data for
     * @param {*} quizData Quiz players object, gotten with quiz.players
     * @param {*} payloadData Payload players array
     */
    matchPlayerQuizData(name, quizData, payloadData) {
        return Object.values(quizData).filter(player => (<any>player)._name == name).map(player => {
            return {
                quizPlayer: player,
                payloadPlayer: payloadData.filter(pp => pp.gamePlayerId == (<any>player).gamePlayerId)[0]
            };
        })[0];
    }
    preInit() {
        this.base = indexbot_base;
        this.include(this, "https://cdn.jsdelivr.net/gh/marcelodolza/iziToast@1.4.0/dist/js/iziToast.js");
        this.includeExternalCSS(this, "https://cdn.jsdelivr.net/gh/marcelodolza/iziToast@1.4.0/dist/css/iziToast.css");
    }
    init(dependencies) {
        this.base.injectDependency(this, dependencies, "settings", "settingsModule");
        if (this.settingsModule) {
            this.settingsModule.registerSetting({ module: this, name: "Debug Mode", description: "Controls debug output", id: "debug", value: false, type: this.settingsModule.SETTING_TYPE.BOOL, onchange: null });
        }
    }
    async postInit() {
        if (this.settingsModule) {
            this.settings = await this.settingsModule.getSettings(this);
        }
    }
}
















indexbot_base.registerModule("Indexbot Common", "common", "A set of common functions for every module.", new IndexbotCommon(), [], ["settings"]);