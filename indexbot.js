// ==UserScript==
// @name IndexBOT
// @namespace indexBOT
// @match https://animemusicquiz.com/
// @grant GM_setClipboard
// @grant GM_notification
// @grant GM_addStyle
// ==/UserScript==
/**
 * AMQ tweak tools/bot based on AMQ-Data-Viewer by LollipopFactory. Most of the code from that project has been cut out due to the differing scope of this project.
 * Should be added as a *monkey script, the usage of GM_setClipboard and GM_notification requires it (can be replaced with default browser methods but clipboard in particular may be funky)
 * Features:
 *  - A running, persistent database of all seen songs by URL and their associated anime.
 *  - How often you've personally seen a song and gotten it correct is tracked and displayed next to song name as a percentage.
 *  - lobbyObserver mode, in which the bot will automatically start the lobby whenever a quiz ends. Untested when non-host. Good for collecting songs in the library. Toggle with F2.
 *  - The name of the song after the answer is revealed is made into a link to the aniList page for that show. This may be incorrect if AniList gives the wrong result.
 * Cheat Modes - These modes disable tracking of number of times you've seen or answered a song, to prevent skewing data.
 *  - Auto Answer - Toggle with "7" on the keyboard
 *      - The bot will automatically put down the correct answer for any song it recognizes. This is done by URL, so if a new version gets uploaded your bot will need to see it first.
 *      - This is customizable with a randomized delay time before submitting an answer.
 *      - You can also choose the percentage of songs you want it to get correct. If it knows a given song and a random number rolled is above this amount, it will input a random anime instead.
 *      - Due to the way it grabs the song URL, it may miss some if the connection is bad. I've tried to make it as quick as possible to get the URL.
 *  - Notifications - Presents any known answers as a notification after grabbing the URL. Clicking on the notification will copy the anime name. 
 *      - If index doesn't know the anime, it will show "no idea".
 *      - Toggle with "5" on the keyboard.
 *  - Reveal Video Early - Will remove the hidden panel over the videoplayer. Not very useful with mp3s.
 * Grimoire Servers
 *  - Grimoire servers allow multiple Index-bot clients to sync their databases together with eachother.
 *  - By running amq.syncSongs() in the console after the script starts, the client will attempt to open a websocket with GRIMOIRE_HOST and sync song databses, authenticating with GRIMOIRE_USER and GRIMOIRE_PASSWORD
 *  - Secure Web Sockets are not yet supported but may be in the future. You may need to edit your firefox config to allow ocnnecting to the non secure socket.
 *  - Syncing involves uploading the list of songs on the client, then downloading the new server's list.
 *  - You can either run your own Grimoire server or connect to one you have a valid username/password for.
 * 
 * NOTES:
 * I recommend not using cheat modes when playing in random lobbies, it spoils the fun for everyone. I primarily made auto answer to get the winter recolors before they disappeared in solo lobbies.
 * Absolutely do not use this in ranked please. As stated, this was not intended for cheating in real, PVP matches. It's intended for qol features like correctness percentages, and anlist links, as well as solo afk note farming.
 * This script relies on your browser supporting IndexedDB, and keeping its data. If it gets regularly wiped due to space issues, many features will be useless.
 * You'll want a CORS bypass extension to get the cheat modes working. I've heard GM_xmlHttpRequest ignores CORS but haven't tried it.
 * Start the bot by opening console with F12 then typing amq.start();. Follow by pressing the above keybinds. Showing correctness percentages is always on unless a cheat is on.
 * 
 * DEV Notes:
 * There seems to be another anticheat check in the form of getVolumeAtMax(), as it actually checks if there's more than 1 listener (listner?) watching for a "next video info"
 * command. The script doesn't use listeners for that, but it's something we may want to bypass in the future if we touch them more.
 */


// main script variables
var autoAnswerOn = false;
var notifications = false;
var answerFound = true; // could be moved into the amq object, user should probably not touch this though it may not matter???
var randomizedDelay = true;
var revealVideoEarly = false;
var animeAsBackground = true;
const AMQ_STYLESHEET = "https://animemusicquiz.com/css/main.css";
const MIN_DELAY = 5;
const MAX_DELAY = 11;
const PERCENT_CORRECT = .95;
const PLAYER_NAME = "yourname"; // Used to grab whether you got the anime correct

const GRIMOIRE_HOST = "ws://localhost:8080";
const GRIMOIRE_USER = "test";
const GRIMOIRE_PASSWORD = "test";
/**
 * Should be called with await to have async functions wait @param ms milliseconds
 * @param {*} ms 
 */
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

/**
 * Gives a random number between min-max, including max
 * @param {*} min 
 * @param {*} inclusiveMax 
 */
function randomInt(min, inclusiveMax) {
    inclusiveMax++;
    inclusiveMax = inclusiveMax - min;
    return Math.floor((Math.random() * Math.floor(inclusiveMax)) + min);
}

amq = {
    songData: [],
    roundData: [],
    currentRoundNum: 0,
    currentSongNum: 0,
    playerData: {},
    db: {},
    guessOnWrong: true,
    lobbyObserverOn: false,
};



/**
 * Constructor for a SongData object. May be updated in the future to hold more song data
 */
class SongData {
    constructor(songName, animeName) {
        this.songName = songName;
        this.animeName = animeName;
        this.timesSeen = 0;
        this.timesCorrect = 0;
    }
}

amq.anilistQuery = function(query, variables, callback) {
    var url = 'https://graphql.anilist.co',
    options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        body: JSON.stringify({
            query: query,
            variables: variables
        })
    };

    fetch(url, options).then(function(response) {
        return response.json().then(function(json) {
            return response.ok ? json : Promise.reject(json);
        })
    }).then(function(json) {
        callback(json.data.Media);
    }).catch(function(error) {
        console.log(error);
    });
}

/**
 * Send a graphql query to anilist to fetch a specific anime's id, which we can use in a predetermined link format to get a link to a specific anime
 */
amq.getAnilistAnimeID = function (animeName, callback) {
    // GraphQL query. We send a search request and look at ID for result. Title is added in for possible error checking?
    var query = `query ($search: String) { 
        Media (search: $search, type: ANIME) {
          id
          title {
            english
          }
        }
      }`;

      // We put animeName is the search query.
    var variables = {
        search: animeName
    }

    amq.anilistQuery(query, variables, (payload) => {
        callback(payload.id);
    });
}

amq.getAnilistImage = function(animeId, callback) {
    var query = `query ($id: Int) {
        Media (id: $id, type: ANIME) {
            coverImage {
                extraLarge
            }
        }
    }`;

    var variables = {
        id: animeId
    }

    amq.anilistQuery(query, variables, (payload) => {
        callback(payload.coverImage.extraLarge);
    });
}

/**
 * Takes an anime name element in the dom, gets the anilist link to the given anime name, and makes the name into a link to the anilist page
 */
amq.insertAnilistLinkIntoDom = function(animeNameElement, id) {
    var animeName = animeNameElement.textContent;
    animeNameElement.textContent = "";
    var aniLink = document.createElement('a');
    aniLink.setAttribute('href', "https://anilist.co/anime/" + id);
    aniLink.setAttribute('target', "_blank");
    aniLink.innerHTML = animeName;
    animeNameElement.appendChild(aniLink);
}

/**
 * Updates number of times a given songname has been seen in the songData objectstore.
 * If the song doesnt already exist, it gets added.
 */
amq.sawSong = function (songData, correct) {
    var songDataStore = amq.db.transaction(["songData"], "readwrite").objectStore("songData");
    var request = songDataStore.get(songData.songName);
    request.onsuccess = function (event) {
        var data = event.target.result;
        if (data == null) {
            data = songData;
        }
        data.timesSeen++;
        if (correct) {
            data.timesCorrect++;
        }

        // Put updated data back
        var putRequest = songDataStore.put(data);
    }
}

/**
 * Finds a song in the songData objectstore and runs callback on (timescorrect/timesseen) to 2 decimal places.
 * Assumes the song exists in the db, which should always be true, and thus timesSeen should always be > 0.
 */
amq.getSongCorrectPercentage = function (songData, callback) {
    var songDataStore = amq.db.transaction("songData").objectStore("songData");
    var request = songDataStore.get(songData.songName);
    request.onsuccess = function (event) {
        var data = event.target.result;
        var percentage = (data.timesCorrect / data.timesSeen);
        callback(percentage);
    }
}

/**
 * Follows through a given url (assumes its a moeplayer link) and calls callback on the responseurl. 
 * Made for converting the moeplayer webms into the real song links
 * @param {*} url the url to check the redirect on
 * @param {*} callback called on the responseurl of the xmlhttprequest
 */
amq.checkMoeRedirect = function (url, callback) {
    xmlhttp = new XMLHttpRequest();
    var responseurl;
    answerFound = false;

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.responseURL != "" && !answerFound) {
            answerFound = true;
            console.log("Got url: " + xmlhttp.responseURL);
            responseurl = xmlhttp.responseURL;
            callback(responseurl);
        }
    }

    xmlhttp.open("GET", url);
    xmlhttp.setRequestHeader("Accept", "video/webm,video/ogg,video/*;q=0.9,application/ogg;q=0.7,audio/*;q=0.6,*/*;q=0.5")
    //xmlhttp.setRequestHeader("Accept-Encoding", "") // Forbidden header name, maybe there's another way but the "clean" request doesnt include this header
    xmlhttp.send();
}

/**
 * An override for AMQ's default starloading function for moeplayer that fetches the real redirect url, stores it for us and loads that into the video player
 */
loadVideoOverride = function() {
    let videourl = "https://animemusicquiz.com" + this.getVideoUrl();
    amq.checkMoeRedirect(videourl, function(url) {
        this.player.src(url)
    }.bind(this));
}

/**
 * CurrentVideoPlaying override. AMQ has a sneaky little bit that checks if the video is currently obscured. We want ot override it so it's alays yes.
 * It only seems to get checked when submitting an answer which should only be done while the video is obscured so it should be fine to do this?
 * I'm sure we'll find out
 */
currentVideoPlayingOverride = function() {
    return true
}

/**
 * Begins the IndexedDB database connection
 */
amq.beginDatabaseConnection = function () {
    var request = window.indexedDB.open("AMQ_Library", 2);
    request.onerror = function (event) {
        alert("Why didn't you allow my web app to use IndexedDB?!");
    };
    request.onsuccess = function (event) {
        amq.db = event.target.result;
        console.log("Loaded DB connection")
    };
    request.onupgradeneeded = function (event) {
        var db = event.target.result;

        if (event.oldVersion < 1) {
            var songStore = db.createObjectStore("songs", { keyPath: "url" });
            songStore.createIndex("anime", "anime", { unique: false });
        }

        if (event.oldVersion < 2) {
            var songdataStore = db.createObjectStore("songData", { keyPath: "songName" });
            songdataStore.createIndex("animeName", "animeName", { unique: false });
            songdataStore.createIndex("timesSeen", "timesSeen", { unique: false });
            songdataStore.createIndex("timesCorrect", "timesCorrect", { unique: false });
        }
    }

}

/**
 * Checks if a given songurl exists in the db, and if not, stores it with the associated animeName
 */
amq.storeSongInDB = function (songurl, animename) {
    var transaction = amq.db.transaction(["songs"], "readwrite");
    var objectStore = transaction.objectStore("songs");
    var getRequest = objectStore.get(songurl);
    var song = {
        url: songurl,
        anime: animename
    }
    getRequest.onsuccess = function (event) {
        if (event.target.result == null) {
            var addRequest = objectStore.add(song);
            addRequest.onsuccess = function (event) {
                console.log(event.target.result + " added to db.");
            }
        }
        else {
            console.log(getRequest.result.anime + " already in db");
        }
    }
}

/**
 * Returns an anime associated with the given songurl in the DB, or null.
 */
amq.getSongInDB = function (songurl, callback) {
    var transaction = amq.db.transaction(["songs"]);
    var objectStore = transaction.objectStore("songs");
    var request = objectStore.get(songurl);
    request.onsuccess = function (event) {
        callback(request.result)
    }
}

/**
 * Gets the full list of songs in your indexeddb
 */
amq.getAllSongs = function(callback) {
    var songs = [];
    var transaction = amq.db.transaction(["songs"]);
    var objectStore = transaction.objectStore("songs");
    var request = objectStore.getAll();
    request.onsuccess = function(event) {
        songs = request.result;
        callback(songs);
    }
}

/**
 * Returns either idk or a random known show
 */
amq.guess = function (callback) {
    var transaction = amq.db.transaction(["songs"]);
    var objectStore = transaction.objectStore("songs");
    var countRequest = objectStore.count();
    var numSongs = 0;
    countRequest.onsuccess = function () {
        numSongs = countRequest.result;
        if (numSongs == 0) {
            callback("idk");
            return;
        }
        else {
            objectStore.openCursor().onsuccess = function (event) {
                var cursor = event.target.result;
                if (cursor) {
                    cursor.advance(Math.floor(Math.random() * numSongs));
                    callback(cursor.value.anime);
                }
            }
        }
    }
}
/**
 * Initiates a connection to the grimoire server defined at the top of this file.
 */
amq.connectToGrimoire = function(callback) {
    var websocket = new WebSocket(GRIMOIRE_HOST);
    websocket.onopen = function(event) {
        console.log("Opened websocket to Grimoire Server");
        callback(websocket);
    }
}
/**
 * Sends a specific command and data payload to the websocket given, presumably conencted to a grimoire server
 */
amq.sendGrimoireCommand = function(websocket, webcommand, datatosend) {
    var jsoncommand = {
        data : datatosend,
        command : webcommand,
        username : GRIMOIRE_USER,
        password : GRIMOIRE_PASSWORD
    }
    var jsontosend = JSON.stringify(jsoncommand);
    websocket.send(jsontosend);
    console.log("Sent grimoire command, awaiting response...");
}

/**
 * Adds any song in the songs array given to the database if it doesnt already exist. Will not update mismiatched song urls with new anime names.
 */
amq.updateSongs = function(songs) {
    for(var i=0;i<songs.length;i++) {
        //console.log("song url: " + songs[i].url + " | anime: " + songs[i].anime);
        amq.storeSongInDB(songs[i].url, songs[i].anime);
    }
}

/**
 * Opens a grimoire websocket connection, awaits a song list, then updates the database, followed by returning the new set of songs to the grimoire to update it.
 */
amq.syncSongs = function() {
    amq.connectToGrimoire(function(websocket) {
        websocket.onmessage = function(event) {
            if(event.data != null) {
                var data = JSON.parse(event.data);
                if(data.error != null && data.error != 0) {
                    console.log(data.data);
                }
                else {
                    amq.updateSongs(JSON.parse(data.data));
                }
                websocket.close();
            }
        }
        amq.getAllSongs(function(songs) {
            amq.sendGrimoireCommand(websocket, "sync", JSON.stringify(songs));
        });
    });
}

/**
 * Imports the current expand library songs available for your list into the database.
 */
amq.importExpandList = function() {
    let expandAnswerListener = new Listener("expandLibrary questions", function (payload) {
        payload.questions.forEach(anime => {
            anime.songs.forEach(song => {
                for (const resolution in song.examples) {
                    amq.storeSongInDB(song.examples[resolution], anime.name)
                }
            });
        });
    });
    expandAnswerListener.bindListener();
    socket.sendCommand({
        type: "library",
        command: "expandLibrary questions"
    });
}


/**
 * Sends the answer to the current video as a notification if it's known.
 */
amq.sendCurrentAnswerNotification = function () {
    let copiedText = "https://animemusicquiz.com" + quizVideoController.getCurrentPlayer().getVideoUrl();
    copiedText = amq.checkMoeRedirect(copiedText, function (resultUrl) {
        amq.getSongInDB(resultUrl, function (song) {
            amq.sendAnswerNotification(song);
        })
    });
}

/**
 * Sends a browser notification containing the anime associated with the given song object. Can be clicked to copy the anime.
 */
amq.sendAnswerNotification = function (song) {
    var notification = null;
    var answer = (song == null) ? null : song.anime;
    if (answer == null) {
        answer = "no idea";
    }
    if (!("Notification" in window)) {
        console.log("This browser does not support desktop notification");
        return;
    }

    if (!notifications) {
        return;
    }

    GM_notification(answer, "AMQ Index-bot: Song Answer", "https://vignette.wikia.nocookie.net/thejusticeworld/images/a/ae/Index.png", function () {
        GM_setClipboard(answer);
    });
}

amq.nameFromAvatarElem = function (elem) {
    return elem.id.substr(9);
}

amq.start = function () {
    clearTimeout(afkKicker.afkWarningTimeout);
    clearTimeout(afkKicker.hostAfkWarningTimeout);
    amq.beginDatabaseConnection();
    let newRound = function () {
        amq.currentRoundNum += 1;
        amq.roundData[amq.currentRoundNum] = {
            songIDs: [],
            playerNames: [],
            correctGuesses: [],
            guesses: [],
            scores: {},
        };
        //get the players in this round
        let avatarElems = document.getElementsByClassName("qpAvatarContainer");
        let playerNum;
        for (playerNum = 0; playerNum < avatarElems.length; playerNum += 1) {
            amq.roundData[amq.currentRoundNum].playerNames.push(amq.nameFromAvatarElem(avatarElems[playerNum]));
        }
    }
    //initialize round data
    newRound();
    //observe the anime title panel, waiting for the name reveal
    const nameHider = document.getElementById('qpAnimeNameHider');
    const songCount = document.getElementById('qpCurrentSongCount');
    const qpAnswer = document.getElementById('qpAnswerInput');
    MoeVideoPlayer.prototype.startLoading = loadVideoOverride; // Override the startloading stuff to pass in a real link to the player
    QuizVideoController.prototype.currentVideoPlaying = currentVideoPlayingOverride // Override the isplaying check to return true always

    if(animeAsBackground) {
        let template = $(`<div id="custom-background"></div>`);
        $('#mainContainer').append(template);
    }

    amq.observer = new MutationObserver(function () {
        if (nameHider.classList.contains("hide")) {
            //when the name is revealed, record the song data
            let animeNameElement = document.getElementById("qpAnimeName");
            let songName = document.getElementById("qpSongName").textContent;
            var songData = new SongData(songName, animeNameElement.textContent);
            // check if the song exists in the IndexDB, if not, add it
            amq.storeSongInDB(document.getElementById("qpSongVideoLink").href, document.getElementById("qpAnimeName").textContent);
            // Reset afk timers
            afkKicker.resetTimers();
            //determine song number, and if we've advanced a round
            //this can be inaccurate if the tracker was not running between rounds, but you're losing data in that case anyway
            const songNum = parseInt(songCount.textContent);
            if (songNum <= amq.currentSongNum) {
                newRound();
            }
            // Insert the link after we've used the name for anything
            amq.getAnilistAnimeID(animeNameElement.textContent, (id) => {
                let animeId = id;
                amq.insertAnilistLinkIntoDom(animeNameElement, animeId);
                if(animeAsBackground) {
                    amq.getAnilistImage(animeId, (pictureLink) => {
                        let styleSheets = document.styleSheets
                        console.log(styleSheets)
                        for(var i = 0;i<styleSheets.length;i++) {
                            if(styleSheets[i].href == AMQ_STYLESHEET) {
                                let style = styleSheets[i]['cssRules'][847].style
                                style["background-image"] = "url(\"" + pictureLink + "\")"
                                style["background-size"] = "100% auto"
                                style["background-attachment"] = "fixed"
                                style["background-position"] = "0px"
                                style["backdrop-filter"] = "blur(3px)"
                            }
                        }
                    })
                }
            });

            // ---- Do not use animeNameElement for anything past this point ----
            amq.currentSongNum = songNum;
            const curRound = amq.roundData[amq.currentRoundNum];
            curRound.songIDs[songNum] = amq.songData.length - 1;
            //see who got it correct
            let avatarElems = document.getElementsByClassName("qpAvatarContainer");
            let corrects = [];
            let myNumber;
            let playerNum;
            for (playerNum = 0; playerNum < avatarElems.length; playerNum += 1) {
                const name = amq.nameFromAvatarElem(avatarElems[playerNum]);
                console.log(name);
                // gets the player number for our guy to keep track of songs seen/answered  
                if (name == PLAYER_NAME) {
                    console.log(name + " is player");
                    myNumber = playerNum;
                }
                const correct = avatarElems[playerNum].getElementsByClassName("qpAvatarAnswerContainer")[0].classList.contains("rightAnswer");
                corrects.push(correct);
            }
            // Increments the number of times we've seen a song and possibly number of times we got it correct. But only if we're not cheating.
            if (!(autoAnswerOn || notifications)) {
                amq.sawSong(songData, corrects[myNumber]);
                // Updates the song name with number of times we've gotten it correct as percentage of total times
                amq.getSongCorrectPercentage(songData, function (percentage) {
                    document.getElementById("qpSongName").textContent += (" (" + (percentage * 100) + "%)");
                });
            }
        }
    });
    amq.observer.observe(nameHider, { attributes: true, childList: false });

    /**
     * Observer for checking if the current quiz has ended, and starts a new game immediately if so. Untested behavior when not hosting.
     */
    const quizPage = document.getElementById('quizPage');
    amq.lobbyObserver = new MutationObserver(function () {
        if (quizPage.classList.contains("hidden")) {
            console.log("Starting new lobby");
            document.getElementById('lbStartButton').click();
        }
    });

    /**
     * Checks to see if the answer field is not disabled. Checks to moeredirect, gets the song in db, and if enabled, waits a delay before answering.
     * Should be cleaned up into separate, neater functions at some point.
     */
    amq.answerObserver = new MutationObserver(function () {
        if (!qpAnswer.disabled && (notifications || autoAnswerOn)) {
            let url = quizVideoController.getCurrentPlayer().player.currentSrc();
            amq.getSongInDB(url, async function (song) {
                let delay = randomInt(MIN_DELAY, MAX_DELAY);

                if (notifications) {
                    amq.sendAnswerNotification(song);
                }

                // Auto answer the song if autoAnswer is on
                if (autoAnswerOn) {
                    if (song == null || Math.random() > PERCENT_CORRECT) {
                        qpAnswer.value = amq.guess(function (animeName) {
                            qpAnswer.value = amq.guessOnWrong == true ? animeName : "";
                            console.log(song == null ? ("Couldn't find song for " + url + ". Guessing.") : "Intentionally choosing incorrect answer.");
                        });
                    }
                    else {
                        qpAnswer.value = song.anime;
                        console.log("Answer found. Anime [" + song.anime + "] matched with url {" + url + "}");
                    }
                    if (randomizedDelay) {
                        console.log("Waiting " + delay + " seconds to answer");
                        await sleep(delay * 1000);
                    }
                    quiz.answerInput.submitAnswer(true)
                }
            });
        }
        if (!qpAnswer.disabled && revealVideoEarly) {
            quizVideoController.getCurrentPlayer().show();
            quiz.videoOverlay.hide();
        } 
    });
    amq.answerObserver.observe(qpAnswer, { attributes: true, childList: false });

    //add a listener to copy some info from the page to the clipboard when a key is pressed
    window.onkeyup = function (e) {
        let key = e.keyCode ? e.keyCode : e.which;
        //check specifically for pressing the `~ key
        if (key == 192) {
            sendCurrentAnswerNotification();
        }
        // f2 starts the autoplay bot
        if (key == 113) {
            // quizpage - check if has hidden class, if so, fire lobby.fireMainbuttonevent()
            if (amq.lobbyObserverOn) {
                amq.lobbyObserver.disconnect();
                console.log("Disconnected lobby observer.")
                amq.lobbyObserverOn = false;
            }
            else {
                amq.lobbyObserver.observe(quizPage, { attributes: true, childList: false });
                console.log("Observation started");
                amq.lobbyObserverOn = true;
            }
        }
        //quizVideoController.getCurrentPlayer().getVideoUrl(); gets the /moeplayer bit
        // 5 toggles answer notifications
        if (key == 53) {
            notifications = !notifications;
            console.log(notifications ? ("Enabled answer notifications") : ("Disabled answer notifications"))
        }
        // 7 starts the auto-answer for known songs
        if (key == 55) {
            if (autoAnswerOn) {
                console.log("Disabled auto-answer");
                autoAnswerOn = false;
            }
            else {
                console.log("Beginning auto-answer");
                autoAnswerOn = true;
            }
        }
    };
};

amq.quit = function () {
    amq.observer.disconnect();
    amq.lobbyObserver.disconnect();
    amq.answerObserver.disconnect();
}

