// ==UserScript==
// @name IndexBOT Railgun
// @namespace indexBOT
// @match https://animemusicquiz.com/
// @grant GM_setClipboard
// @grant GM_notification
// @grant GM_addStyle
// @grant GM_xmlhttpRequest
// @require https://cdn.jsdelivr.net/gh/marcelodolza/iziToast@1.4.0/dist/js/iziToast.js
// ==/UserScript==
/**
 * AMQ Toolkit based on the original indexbot. Reorganized and rewritten in some places in order to better organize functionality.
 * Contains quality of life functionality such as setting anime backgrounds, linking to the anilist page for a show and tracking specific song guess rates.
 * Also contains tools for cheating, including autoanswer, revealing the video early, and answer notifications.
 * Should be added as a *monkey script.
 * Features:
 *  - A running, persistent database of all seen songs by URL and their associated anime.
 *  - How often you've personally seen a song and gotten it correct is tracked and displayed next to song name as a percentage.
 *  - lobbyObserver mode, in which the bot will automatically start the lobby whenever a quiz ends. Untested when non-host. Good for collecting songs in the library. Toggle with F2.
 *  - The name of the song after the answer is revealed is made into a link to the aniList page for that show. This may be incorrect if AniList gives the wrong result.
 *  - Anime As Background - When an anime is revealed in a quiz, it will try to fetch an image for it from anilist and set that as the background
 * Cheat Modes - These modes disable tracking of number of times you've seen or answered a song, to prevent skewing data.
 *  - Auto Answer - Toggle with "7" on the keyboard
 *      - The bot will automatically put down the correct answer for any song it recognizes. This is done by URL, so if a new version gets uploaded your bot will need to see it first.
 *      - This is customizable with a randomized delay time before submitting an answer.
 *      - You can also choose the percentage of songs you want it to get correct. If it knows a given song and a random number rolled is above this amount, it will input a random anime instead.
 *      - Due to the way it grabs the song URL, it may miss some if the connection is bad. I've tried to make it as quick as possible to get the URL.
 *  - Notifications - Presents any known answers as a notification after grabbing the URL. Clicking on the notification will copy the anime name. 
 *      - If index doesn't know the anime, it will show "no idea".
 *      - Toggle with "5" on the keyboard.
 *  - Reveal Video Early - Will remove the hidden panel over the videoplayer. Not very useful with mp3s.
 * Grimoire Servers
 *  - Grimoire servers allow multiple Index-bot clients to sync their databases together with eachother.
 *  - By running amq.syncSongs() in the console after the script starts, the client will attempt to open a websocket with GRIMOIRE_HOST and sync song databses, authenticating with GRIMOIRE_USER and GRIMOIRE_PASSWORD
 *  - Secure Web Sockets are not yet supported but may be in the future. You may need to edit your firefox config to allow connecting to the non secure socket.
 *  - Syncing involves uploading the list of songs on the client, then downloading the new server's list.
 *  - You can either run your own Grimoire server or connect to one you have a valid username/password for.
 * 
 * NOTES:
 * I recommend not using cheat modes when playing in random lobbies, it spoils the fun for everyone. I primarily made auto answer to get the winter recolors before they disappeared in solo lobbies.
 * Absolutely do not use this in ranked please. As stated, this was not intended for cheating in real, PVP matches. It's intended for qol features like correctness percentages, and anlist links, as well as solo afk note farming.
 * This script relies on your browser supporting IndexedDB, and keeping its data. If it gets regularly wiped due to space issues, many features will be useless.
 * 
 * DEV Notes:
 * There seems to be another anticheat check in the form of getVolumeAtMax(), as it actually checks if there's more than 1 listener (listner?) watching for a "next video info"
 * command. The script doesn't use listeners for that, but it's something we may want to bypass in the future if we touch them more.
 */

// -----Data structures-----
class SongData {
    constructor(songName, animeName) {
        this.songName = songName;
        this.animeName = animeName;
        this.timesSeen = 0;
        this.timesCorrect = 0;
    }
}

class Room {
    constructor(settings, host, hostavatar, id, numplay, numspec, players, inlobby, songleft) {
        this.settings = settings;
        this.host = host;
        this.hostAvatar = hostavatar;
        this.id = id;
        this.numberOfPlayers = numplay;
        this.numberOfSpectators = numspec;
        this.players = players;
        this.inLobby = inlobby;
        this.songLeft = songleft;
    }
}
/**
 * Songs should be in the format of an object:
 * {
 *  anime: String
 *  url: String
 *  guess: String
 *  correct: Bool
 * }
 */
class RoundData {
    constructor() {
        this.correct = 0;
        this.seen = 0;
        this.songs = []
        this.score = 0;
        this.placement = 0;
        this.date = new Date();
    }
    addSong(anime, url, guess, correct) {
        let song = {anime,url,guess,correct};
        this.songs.push(song);
        return song;
    }
}

indexbot = {
    // Settings intended to be changed by the user
    settings: {
        // Cheat settings
        autoAnswer: false,
        answerNotifications: false,
        randomizedAnswerDelay: false,
        autoRevealVideo: false,

        answerMinDelay: 5,
        answerMaxDelay: 11,
        botPercentCorrect: .95,

        // Other settings
        animeAsBackground: false,
        anilistLink: false,
        displaySongPercentage: false,
        endOfRoundReport: false,
        killAFKTimer: false,

        // Grimoire settings
        grimoireHost: "ws://localhost:8080",
        grimoireUser: "index",
        grimoirePassword: "index",

        // Quick join settings
        quickJoinRoomName: "",
        quickJoinRoomPassword: "",
        quickPlaySettings: "",

        //debug logging
        debug: false,

        // version
        version: 3

    },
    // Data held by the bot for internal use.
    data: {
        grimoireConnection: {},
        db: {},
        settingTab: {},
        settingPage: {},
        settingTable: {},
        roundData: new RoundData(),
        rooms: []
    },
    // 
};

// -----Utility functions-----
indexbot.utility = {}
/**
 * Should be called with await to have async functions wait @param ms milliseconds
 * @param {int} ms how long to sleep
 */
indexbot.utility.sleep = function(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

/**
 * Gives a random number between min-max, including max
 * @param {int} min minimum
 * @param {int} inclusiveMax inclusive maximum
 */
indexbot.utility.randomInt = function(min, inclusiveMax) {
    inclusiveMax++;
    inclusiveMax = inclusiveMax - min;
    return Math.floor((Math.random() * Math.floor(inclusiveMax)) + min);
}

indexbot.utility.alert = function(message, messageType) {
    let mapping = {
        info: iziToast.info,
        success: iziToast.success,
        error: iziToast.error,
        warning: iziToast.warning
    }
    messageType = messageType || "info"
    let notificationMethod = mapping[messageType]
    let settings = {
        title: "Indexbot",
        message: message,
        position: "topLeft"
    }
    notificationMethod.call(iziToast, settings)
}


indexbot.utility.loadingAlert = function(text, icon) {
    Swal.fire({
        title: "Indexbot",
        text: text,
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
        showConfirmButton: false,
        type: icon,
    });
}

/**
 * Hacky way to remove accents and other weird marks from a string. normalize changes accented characters to be represented by
 * char + accent, then the replace strips accents/diacritics/etc. 
 * @param {String} msg string to remove accents from
 */
indexbot.utility.stripAccents = function(msg) {
    return msg.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
}

indexbot.utility.debug = function(msg) {
    if(indexbot.settings.debug) {
        console.log(msg)
    }
}

// -----Anilist Queries-----
indexbot.anilist = {}
/**
 * Generic Anilist GraphQL constructor that crafts a GraphQL request based on the query and variables passed in
 * @param {string} query full GraphQL query to be sent to Anilist
 * @param {object} variables json object containing pairings of queryVar = Value
 * @param {function} callback function to call with result payload on success
 */
indexbot.anilist.anilistQuery = function(query, variables, callback) {
    var url = 'https://graphql.anilist.co',
    options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        body: JSON.stringify({
            query: query,
            variables: variables
        })
    };

    fetch(url, options).then(function(response) {
        return response.json().then(function(json) {
            return response.ok ? json : Promise.reject(json);
        })
    }).then(function(json) {
        callback(json.data.Media);
    }).catch(function(error) {
        console.log(error);
    });
}

/**
 * Send a graphql query to anilist to fetch a specific anime's id, which we can use in a predetermined link format to get a link to a specific anime
 */
indexbot.anilist.queries = {}
indexbot.anilist.queries.getAnilistAnimeID = function (animeName, callback) {
    // GraphQL query. Title is included for future error checking
    animeName = indexbot.utility.stripAccents(animeName)
    var query = `query ($search: String) { 
        Media (search: $search, type: ANIME) {
          id
          title {
            english
          }
        }
      }`;

      // We put animeName as the search query.
    var variables = {
        search: animeName
    }

    // Gives the id fetched to the callback
    indexbot.anilist.anilistQuery(query, variables, (payload) => {
        callback(payload.id);
    });
}

/**
 * 
 * @param {*} animeId AnimeID to lookup on Anilist
 * @param {function} callback Function to be called with payload on success
 */
indexbot.anilist.queries.getAnilistImage = function(animeId, callback) {
    // GraphQL query. Uses the animeid to get a link to an image for the anime.
    var query = `query ($id: Int) {
        Media (id: $id, type: ANIME) {
            coverImage {
                extraLarge
            }
        }
    }`;

    var variables = {
        id: animeId
    }

    // Gives the image link to the callback
    indexbot.anilist.anilistQuery(query, variables, (payload) => {
        callback(payload.coverImage.extraLarge);
    });
}

// -----Edits to the html-----


indexbot.html = {}
indexbot.html.input_types = {}
indexbot.html.input_types.CHECKBOX = "checkbox"
indexbot.html.input_types.TEXT = "text"
/**
 * Adds a new tab to AMQ's settings page
 * @param {string} newTabTitleText title of new tab
 */
indexbot.html.addSettingsTab = function(newTabTitleText, pageId) {
    const SETTING_MODAL_ID = "settingModal";
    const NEW_TAB_CLASSES = ["tab", "leftRightButtonTop", "clickAble"];
    const NEW_TAB_ONCLICK = "options.selectTab('" + pageId + "', this)"
    // nasty way of getting the proper container to add to, no id to jump to
    let tabContainer = document.getElementById(SETTING_MODAL_ID).firstElementChild.firstElementChild.firstElementChild.nextElementSibling;
    let newTab = document.createElement("div");
    NEW_TAB_CLASSES.forEach(function(c) {
        newTab.classList.add(c)
    });
    newTab.setAttribute("onclick", NEW_TAB_ONCLICK);
    let newTabTitle = document.createElement("h5");
    newTabTitle.textContent = newTabTitleText;
    newTab.appendChild(newTabTitle)

    tabContainer.appendChild(newTab);
    // reinitializes the options tabs so everything applies properly
    options.$SETTING_TABS = $("#settingModal .tab");
}

/**
 * Adds a new div to the appropriate HTML section to be linked up with a new settings tab
 * @param {string} id HTML id for the added div
 */
indexbot.html.addSettingsPage = function(id) {
    const NEW_PAGE_CLASSES = ["settingContentContainer", "hide", "indexbot-settings"];
    const SETTING_MODAL_ID = "settingModal";
    // nasty way of getting the proper container to add to, no id to jump to
    let pageContainer = document.getElementById(SETTING_MODAL_ID).firstElementChild.firstElementChild.firstElementChild.nextElementSibling.nextElementSibling;
    let newPage = document.createElement("div");
    newPage.setAttribute("id", id);
    NEW_PAGE_CLASSES.forEach((c) => {
        newPage.classList.add(c);
    });

    pageContainer.appendChild(newPage);
    // reinitializes the options pages so everything applies properly
    options.$SETTING_CONTAINERS = $(".settingContentContainer");

    // Initialize the perfect scrollbar 
    $(`#${id}`).perfectScrollbar();
    return newPage;
}

indexbot.html.createTable = function() {
    let table = document.createElement("table");
    table.classList.add("table");
    let tableBody = document.createElement("tbody");
    table.appendChild(tableBody);

    return table
}
indexbot.html.addSetting = function(name, type, table, setting, isNum=false) {
    let row = document.createElement("tr");
    let data1 = document.createElement("td");
    let input = document.createElement("input");
    let label = document.createElement("td");

    input.setAttribute("type", type);
    label.textContent = name;
    row.appendChild(label);
    row.appendChild(data1);
    data1.appendChild(input);

    if (type == indexbot.html.input_types.TEXT) {
        let updateButton = document.createElement("button");
        updateButton.classList.add("btn");
        updateButton.classList.add("btn-primary")
        input.classList.add("form-control-inline");
        input.classList.add("form-control")
        updateButton.textContent = "Update";
        updateButton.onclick = function() {
            indexbot.controller.updateSetting(setting, input.value, isNum);
        }
        data1.append(updateButton)
        input.value = indexbot.settings[setting];
    }
    else {
        input.checked = indexbot.settings[setting];
        input.onclick = function() {
            indexbot.controller.updateSetting(setting, input.checked)
        }
    }

    table.append(row);
    return input;
}

indexbot.html.buildMainSettingsPage = function() {
    const PAGE_ID = "indexbot-settings";
    let pageDiv = this.addSettingsPage(PAGE_ID);
    let tab = this.addSettingsTab("Indexbot", PAGE_ID);
    let table = this.createTable();
    let tableBody = table.firstElementChild
    pageDiv.appendChild(table);

    indexbot.data.settingTab = tab;
    indexbot.data.settingPage = pageDiv;
    indexbot.data.settingTable = tableBody;
}

indexbot.html.insertAnilistIntoDomElement = function(link, anime, element) {
    let anilistLink = document.createElement("a");
    anilistLink.setAttribute("href", link);
    anilistLink.setAttribute("target", "_blank");
    anilistLink.innerHTML = anime;
    element.prop("textContent", "");
    element.append(anilistLink);
}

indexbot.html.insertSongGuessPercentage = function(percentage) {
    let text = " (" + percentage + ")"
    $("#qpSongName").append(text);
}

indexbot.html.createQuickPlayButton = function() {
    let classes = ["button", "floatingContainer", "mainMenuButton"];
    let mainMenu = $("#mainMenu");
    let quickPlayButton = document.createElement("div");
    let quickPlayLabel = document.createElement("h1");
    quickPlayLabel.textContent = "Quick Play";
    quickPlayButton.id = "indexbotQuickPlay";
    classes.forEach((c) => {
        quickPlayButton.classList.add(c);
    });
    quickPlayButton.onclick = indexbot.controller.quickPlay;
    quickPlayButton.appendChild(quickPlayLabel);
    mainMenu.append(quickPlayButton);
}

indexbot.html.addModalWindow = function(title, windowID, headerID, bodyID) {
    let parentContainer = $("#gameContainer");
    let newModal = document.createElement("div");
    let newModalDialog = document.createElement("div");
    let newModalContent = document.createElement("div");
    let newModalHeader = document.createElement("div");
    let newModalBody = document.createElement("div");

    let newModalHeaderCloseButton = document.createElement("button");
    let newModalHeaderCloseButtonSpan = document.createElement("span");
    let newModalHeaderTitle = document.createElement("h2");

    newModal.appendChild(newModalDialog);
    newModalDialog.appendChild(newModalContent);

    newModalHeaderCloseButton.appendChild(newModalHeaderCloseButtonSpan);
    newModalHeader.appendChild(newModalHeaderCloseButton);
    newModalHeader.appendChild(newModalHeaderTitle);

    newModalContent.appendChild(newModalHeader);
    newModalContent.appendChild(newModalBody);
    parentContainer.append(newModal);

    newModal.id = windowID;
    newModalHeader.id = headerID;
    newModalBody.id = bodyID;

    newModal.classList.add("modal", "fade");
    newModal.setAttribute("tabindex", "-1");
    newModal.role = "dialog";

    newModalDialog.classList.add("modal-dialog");
    newModalDialog.role = "document";

    newModalContent.classList.add("modal-content");

    newModalBody.classList.add("modal-body");

    newModalHeader.classList.add("modal-header");

    newModalHeaderCloseButton.classList.add("close");
    newModalHeaderCloseButton.type = "button";
    newModalHeaderCloseButton.setAttribute("data-dismiss", "modal");
    newModalHeaderCloseButton.setAttribute("aria-label", "Close");
    newModalHeaderCloseButtonSpan.setAttribute("aria-hidden", "true");
    newModalHeaderCloseButtonSpan.textContent = "x";
    newModalHeaderTitle.classList.add("modal-title");
    newModalHeaderTitle.textContent = title;

    return newModal;
}

indexbot.html.addRoundOptionButton = function(faIconClass, tooltip, onclick, id) {
    let optionContainer = $("#qpOptionContainer >");
    let newButton = document.createElement("div");
    let newButtonIcon = document.createElement("i");

    newButton.classList.add("clickAble");
    newButton.classList.add("qpOption");
    newButton.classList.add("indexbot-qpicon-width")
    newButton.onclick = onclick;
    newButton.setAttribute("data-toggle", "popover");
    newButton.setAttribute("data-content", tooltip);
    newButton.setAttribute("data-trigger", "hover");
    newButton.setAttribute("data-placement", "bottom");
    newButton.setAttribute("data-original-title", "");
    newButton.setAttribute("title", "");
    newButton.id = id;

    newButtonIcon.classList.add("fa");
    newButtonIcon.classList.add("qpMenuItem");
    newButtonIcon.classList.add(faIconClass);
    newButtonIcon.setAttribute("aria-hidden", "true");

    newButton.appendChild(newButtonIcon);
    optionContainer.append(newButton);

    // Initialize popovers for all indexbot qpoptions.
    $(function () {
        $('.indexbot-qpicon-width').popover();
    });

    return newButton;
}

indexbot.html.initRoundReportHeader = function() {
    indexbot.data.roundReportSubtitleID = "rrheadersubtitle";
    indexbot.data.roundReportSongCountID = "rrheadersongcount";
    indexbot.data.roundReportScoreID = "rrheaderscore";
    indexbot.data.roundReportPlacementID = "rrheaderplacement";
    let rrHeader = $("#"+indexbot.data.roundReportModalHeaderID);
    let rrSubtitle = document.createElement("h3");
    rrHeader.append(rrSubtitle);
    rrSubtitle.outerHTML = `<h3 class="modal-title" id="rrheadersubtitle">Songs: 
    <span id="rrheadersongcount">0</span>
     | Score:
    <span id="rrheaderscore">0</span>
     | Position:
    <span id="rrheaderplacement">0</span>
  </h3>`


}

indexbot.html.initRoundReportTable = function() {
    indexbot.data.roundReportTableID = "rrtable"

    let rrBody = $("#"+indexbot.data.roundReportModalBodyID);
    let table = indexbot.html.createTable();

    table.id = indexbot.data.roundReportTableID;

    rrBody.append(table);

    return table;
}

indexbot.html.initRoundReportBody = function() {
    indexbot.data.roundReportTableBodyID = "rrtablebody"

    let table = indexbot.html.initRoundReportTable();
    let header = document.createElement("thead");
    let body = $("#"+indexbot.data.roundReportTableID + " > tbody");
    let rrBody = $("#"+indexbot.data.roundReportModalBodyID);

    table.prepend(header);
    header.outerHTML = 
    `  <thead>
    <tr>
        <th scope="col">Anime</th>
        <th scope="col">Guess</th>
        <th scope="col">Correct</th>
    </tr>
    </thead>`

    body.prop("id", indexbot.data.roundReportTableBodyID);

    rrBody.addClass("roundReportScrolling");
    rrBody.perfectScrollbar();

}

indexbot.html.showRoundReport = function() {
    $("#"+indexbot.data.roundReportModalID).modal()
}

indexbot.html.addRoundReportEntry = function(entry) {
    let tableBody = $("#"+indexbot.data.roundReportTableBodyID);
    let row = document.createElement("tr");
    let animeName = document.createElement("td");
    let animeLink = document.createElement("a");
    let guess = document.createElement("td");
    let correct = document.createElement("td");
    let correctCheck = document.createElement("i");

    animeName.appendChild(animeLink);
    animeLink.href = entry.url;
    animeLink.textContent = entry.anime;
    animeLink.target = "_blank";

    guess.textContent = entry.guess;

    correct.appendChild(correctCheck);
    correctCheck.setAttribute("aria-hidden", true);
    correctCheck.classList.add("fa");
    if(entry.correct) {
        correctCheck.classList.add("fa-check");
    }

    row.appendChild(animeName);
    row.appendChild(guess);
    row.appendChild(correct);
    tableBody.append(row);
}

indexbot.html.clearRoundReportEntries = function() {
    $("#"+indexbot.data.roundReportTableBodyID + " > tr").remove();

}

indexbot.html.updateRoundReportHeader = function(data) {
    $("#"+indexbot.data.roundReportSongCountID).text(data.seen);
    $("#"+indexbot.data.roundReportScoreID).text(data.score);
    $("#"+indexbot.data.roundReportPlacementID).text(data.placement);
}

indexbot.html.updateAndViewRoundReport = function(data) {
    indexbot.html.updateRoundReportHeader(data);
    indexbot.html.showRoundReport();
}
// -----Redirect decoder-----
/**
 * Follows through a given url (assumes its a moeplayer link) and calls callback on the responseurl. 
 * Made for converting the moeplayer webms into the real song links
 * @param {*} url the url to check the redirect on
 * @param {*} callback called on the responseurl of the xmlhttprequest
 */
indexbot.checkMoeRedirect = function (url, callback) {
    let xmlhttp = GM_xmlhttpRequest({
        url: url,
        method: "GET",
        headers: {
            "Accept": "video/webm,video/ogg,video/*;q=0.9,application/ogg;q=0.7,audio/*;q=0.6,*/*;q=0.5"
        }
    })
    let responseurl;
    let answerFound = false;

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.responseURL != "" && !answerFound) {
            answerFound = true;
            responseurl = xmlhttp.responseURL;
            callback(responseurl);
        }
    }

    xmlhttp.open("GET", url);
    //xmlhttp.setRequestHeader("Accept", "video/webm,video/ogg,video/*;q=0.9,application/ogg;q=0.7,audio/*;q=0.6,*/*;q=0.5")
    //xmlhttp.setRequestHeader("Accept-Encoding", "") // Forbidden header name, maybe there's another way but the "clean" request doesnt include this header
    xmlhttp.send();
}

// -----Controller Methods-----
indexbot.controller = {}
indexbot.controller.updateSetting = function(setting, value, isInt=false) {
    indexbot.settings[setting] = isInt ? parseInt(value) : value
    console.log("Setting " + setting + " updated to " + indexbot.settings[setting])
    indexbot.database.savesettings();

}

indexbot.controller.dangerMode = function() {
    return indexbot.settings.answerNotifications || indexbot.settings.autoAnswer || indexbot.settings.autoRevealVideo
}

indexbot.controller.getAnimeName = function(animeNames) {
    return options.useRomajiNames ? animeNames.romaji : animeNames.english;
}

indexbot.controller.updateRoomList = function() {
    return new Promise((resolve, reject) => {
        let roomListListener = new Listener("New Rooms", function (rooms) {
            indexbot.data.rooms = [];
            rooms.forEach(function(room) {
                indexbot.data.rooms.push(new Room(room.settings, room.host, room.hostAvatar, room.id, room.numberOfPlayers, room.numberOfSpectators, room.players, room.inLobby, room.songLeft));
            })
            roomListListener.unbindListener();
            socket.sendCommand({
                type: 'roombrowser',
                command: 'remove roombrowser listners'
            });
            resolve();
        });
        roomListListener.bindListener();
        socket.sendCommand({
            type: "roombrowser",
            command: "get rooms"
        });
    })
}

indexbot.controller.findRoomByName = function(name) {
    return new Promise((resolve, reject) => {
        indexbot.controller.updateRoomList().then(function() {
            let rooms = indexbot.data.rooms;
            let target;
            rooms.forEach(function(room) {
                if (room.settings.roomName == name) {
                    target = room
                }
            });
            resolve(target)
        })
    })
}

indexbot.controller.joinRoom = function(name, password) {
    return new Promise((resolve, reject) => {
        indexbot.controller.findRoomByName(name).then(function(room) {
            if (room == undefined) {
                reject()
                return
            }
            if (room.inLobby) {
                roomBrowser.fireJoinLobby(room.id, password);
                resolve()
            }
            else {
                roomBrowser.fireSpectateGame(room.id, password);
                resolve()
            }
        });
    })
}

indexbot.controller.hostRoom = function(settingsString, name, password) {
    let roomName = name;
    let private = password != "";
    let settings = hostModal._settingStorage._serilizer.decode(settingsString)
    //let settings = hostModal.DEFUALT_SETTINGS // ill worry about cloning in new version
    let command = "host room"
    // Save codes dont include room name, password or gamemode. Using standard gamemode to save time for now
    settings.roomName = roomName
    settings.privateRoom = private
    settings.password = password
    settings.gameMode = "Multiplayer"
    //Object.keys(loadedSettings).forEach(settingKey => {
    //    settings[settingKey] = loadedSettings[settingKey]
    //})

    let hostListener = new Listener("Host Game", function (response) {
		lobby.setupLobby(response, false);
		viewChanger.changeView("lobby");
		hostListener.unbindListener();
    }.bind(this));
    
    hostListener.bindListener();
    socket.sendCommand({
		type: 'roombrowser',
		command: command,
		data: settings
	});
}

indexbot.controller.quickHost = function() {
    if (indexbot.settings.quickPlaySettings == "") {
        indexbot.utility.alert("Please set up room settings first in Indexbot settings", "error");
        return false
    }
    indexbot.controller.hostRoom(indexbot.settings.quickPlaySettings, indexbot.settings.quickJoinRoomName, indexbot.settings.quickJoinRoomPassword);
    Swal.close()
}

indexbot.controller.quickPlay = function() {
    if (indexbot.settings.quickJoinRoomName == "") {
        indexbot.utility.alert("Please set up a room name in settings first", "error");
        return false;
    }
    indexbot.utility.loadingAlert("Querying room data...")
    let name = indexbot.settings.quickJoinRoomName
    let password = indexbot.settings.quickJoinRoomPassword == "" ? undefined : indexbot.settings.quickJoinRoomPassword
    indexbot.controller.joinRoom(name, password).then(Swal.close, indexbot.controller.quickHost)
}

indexbot.controller.quickJoinSetup = function() {
    indexbot.html.createQuickPlayButton();

    indexbot.html.addSetting("Quick Play Room Name", indexbot.html.input_types.TEXT, indexbot.data.settingTable, "quickJoinRoomName");
    indexbot.html.addSetting("Quick Play Password", indexbot.html.input_types.TEXT, indexbot.data.settingTable, "quickJoinRoomPassword");
    indexbot.html.addSetting("Quick Play Settings Code", indexbot.html.input_types.TEXT, indexbot.data.settingTable, "quickPlaySettings");
}

indexbot.controller.addDebugOption = function() {
    indexbot.html.addSetting("Debug Mode", indexbot.html.input_types.CHECKBOX, indexbot.data.settingTable, "debug");
}

indexbot.controller.findPlayerData = function(name, quizPlayerData, payloadPlayerData) {
    let playerID;
    let results;
    Object.values(quizPlayerData).forEach(player => {
        if(player._name == name) {
            playerID = player.gamePlayerId
        }
    })
    playerResults = payloadPlayerData[playerID]
    return [playerID, playerResults]
}

indexbot.controller.initRoundReport = function() {
    indexbot.data.roundReportModalID = "indexbotrrwindow";
    indexbot.data.roundReportModalHeaderID = "indexbotrrheader";
    indexbot.data.roundReportModalBodyID = "indexbotrrbody";
    indexbot.html.addModalWindow("Round Report", indexbot.data.roundReportModalID, indexbot.data.roundReportModalHeaderID, indexbot.data.roundReportModalBodyID);

    let callback = function() {
        indexbot.html.updateAndViewRoundReport(indexbot.data.roundData);
    }

    indexbot.html.addRoundOptionButton("fa-book", "Round Report", callback, "roundreportbutton");
    indexbot.listeners.initEndOfRoundReportListener();
    indexbot.html.initRoundReportHeader();
    indexbot.html.initRoundReportBody();
}

// -----Database Stuff-----
indexbot.database = {}

/**
 * Opens a new connection to the browser's IndexedDB database. Connects to "IndexBotDB" and performs all necessary setup. Stores the connection on success. 
 */
indexbot.database.beginDatabaseConnection = function () {
    return new Promise((resolve, reject) => {
        let request = window.indexedDB.open("IndexBotDB", 2);
        request.onerror = function (event) {
            alert("Error connecting to IndexBotDB. Check you allow IndexedDB connections!");
            reject("failure");
        };
        request.onsuccess = function (event) {
            indexbot.data.db = event.target.result;
            resolve("success");
        };
        request.onupgradeneeded = function (event) {
            const db = event.target.result;
    
            if (event.oldVersion < 1) {
                const songStore = db.createObjectStore("songs", { keyPath: "url" });
                songStore.createIndex("anime", "anime", { unique: false });
            }
    
            if (event.oldVersion < 2) {
                const songdataStore = db.createObjectStore("songData", { keyPath: "songName" });
                songdataStore.createIndex("animeName", "animeName", { unique: false });
                songdataStore.createIndex("timesSeen", "timesSeen", { unique: false });
                songdataStore.createIndex("timesCorrect", "timesCorrect", { unique: false });
            }
    
            if (event.oldVersion < 3) {
                const settingsStore = db.createObjectStore("settings", {keyPath: "version"});
                settingsStore.transaction.oncomplete = function(event) {
                    const transaction = db.transaction("settings", "readwrite");
                    const settingsStore = transaction.objectStore("settings");
                    settingsStore.add(indexbot.settings);
                }
            }
        }
    })
}

indexbot.database.getObjectStore = function(objectStoreName) {
    const transaction = indexbot.data.db.transaction(objectStoreName, "readwrite");
    const objectStore = transaction.objectStore(objectStoreName);
    return objectStore;
}

/**
 * Stores a new object in the bot's IndexedDB connection containing the songurl and the anime its associated with.
 * @param {string} songurl URL to the song
 * @param {string} animename Anime associated with song
 */
indexbot.database.storeSongInDB = function (songurl, animename) {
    objectStore = this.getObjectStore("songs");
    const getRequest = objectStore.get(songurl);
    const song = {
        url: songurl,
        anime: animename
    }
    getRequest.onsuccess = function (event) {
        if (event.target.result == null) {
            const addRequest = objectStore.add(song);
            addRequest.onsuccess = function (event) {
                console.log(event.target.result + " added to db.");
            }
        }
        else {
            console.log(getRequest.result.anime + " already in db");
        }
    }
}

indexbot.database.bulkStoreSongs = function(songs) {
    let objectStore = this.getObjectStore("songs");
    songs.forEach(function(song) {
        objectStore.put(song)
    })
}

/**
 * Looks for a given song in the IndexedDB store by the URL and returns an object containing the URL and anime name
 * @param {string} songurl URL of the song to look up
 * @param {function} callback Function to call on success with the SongData object result
 */
indexbot.database.getSongInDB = function (songurl, callback) {
    const transaction = indexbot.data.db.transaction(["songs"]); //readonly
    const objectStore = transaction.objectStore("songs");
    const request = objectStore.get(songurl);
    request.onsuccess = function (event) {
        callback(request.result)
    }
}

indexbot.database.loadsettings = function() {
    return new Promise((resolve, reject) => {
        const settingStore = indexbot.database.getObjectStore("settings");
        const request = settingStore.get(indexbot.settings.version);
        request.onsuccess = function(event) {
            for (setting in request.result) {
                if (setting == "version") {
                    continue
                }
                indexbot.settings[setting] = request.result[setting]
            }
            resolve("success")
        }
    })
}

indexbot.database.savesettings = function() {
    const settingStore = this.getObjectStore("settings");
    const request = settingStore.put(indexbot.settings);
    request.onsuccess = function(event) {
        indexbot.utility.alert("Saved settings!");
    }
}

indexbot.database.sawSong = function(songData, correct) {
    let songDataStore = indexbot.database.getObjectStore("songData");
    let request = songDataStore.get(songData.songName);
    request.onsuccess = function(event) {
        let data = event.target.result
        if(data == null) {
            data = songData
        }
        data.timesSeen += 1
        if (correct) {
            data.timesCorrect += 1
        }
        let putRequest = songDataStore.put(data);
    }
}

indexbot.database.getSeenSong = function(songName, callback) {
    let songDataStore = indexbot.database.getObjectStore("songData");
    let request = songDataStore.get(songName);
    request.onsuccess = function(event) {
        let data = event.target.result
        callback(data);
    }
}
// -----Styling Stuff-----
indexbot.styles = {}

indexbot.styles.addStaticStyles = function() { 
    GM_addStyle ( `
    .tab-modal .tabContainer {
        background-color: #1b1b1b;
        height: auto;
        overflow: hidden;
    }
    .form-control-inline {
        width: 50%;
        display: inline;
    }
    #mainMenu > .button {
        margin-bottom: 1vh;
    }
    .table > tbody > tr:first-child {
        background-color: initial;
    }
    .indexbot-settings {
        max-height: 330px;
        overflow: hidden;
        position: relative;
    }
    .roundReportScrolling {
        position: relative;
        overflow: hidden;
        max-height: 600px;
    }
    #qpOptionContainer {
        padding-left: 10px;
        width: auto;
    }
    .indexbot-qpicon-width {
        width: 30px;
        height: 100%;
    }
`);

    $("head").append (
        '<link '
    + 'href="https://cdn.jsdelivr.net/gh/marcelodolza/iziToast@1.4.0/dist/css/iziToast.css" '
    + 'rel="stylesheet" type="text/css">'
    );
}

indexbot.styles.addDynamicStyles = function() {
    // Adding rule for anime as background. Empty to start.
    const stylesheet_id = document.styleSheets.length - 1;
    const stylesheet = document.styleSheets[stylesheet_id];
    const styleRuleID = stylesheet.insertRule(
        `
    #indexbotAnilistImage {
    }`, stylesheet.rules.length);

    indexbot.data.game_stylesheet = stylesheet
    indexbot.data.game_styleRuleID = styleRuleID

}

/**
 * Adds a new div element next to qpoptioncontainer that displays the needed background image. Style handled by #indexbotAnilistImage
 */
indexbot.styles.addAnimeImageBGDiv = function() {
    let targetDiv = $("#qpOptionContainer");
    let newDiv = document.createElement("div");
    indexbot.data.animeAsBGDiv = newDiv;
    newDiv.id = "indexbotAnilistImage";
    targetDiv.after(newDiv);
}

indexbot.styles.setGameBG = function(url) {
    const style = indexbot.data.game_stylesheet.cssRules[indexbot.data.game_styleRuleID].style
    style["background-image"] = "url(\"" + url + "\")"
    style["background-size"] = "75% 100%"
    style["background-attachment"] = "fixed"
    style["background-position"] = "0px"
    style["filter"] = "blur(3px)"
    style["height"] = "120%"
    style["width"] = "120%"
    style["margin"] = "0 -20px"
    style["position"] = "absolute"
    style["z-index"] = "0"

}

indexbot.styles.clearGameBG = function() {
    const style = indexbot.data.game_stylesheet.cssRules[indexbot.data.game_styleRuleID].style
    style["background-image"] = ""
    style["background-size"] = ""
    style["background-attachment"] = ""
    style["background-position"] = ""
    style["filter"] = ""
    style["height"] = ""
    style["width"] = ""
    style["margin"] = ""
    style["position"] = ""
    style["z-index"] = ""
}

// -----Function Overrides-----
indexbot.overrides = {}
// Overrides for three functions here, along with a function to perform the override

/**
 * Overrides the load video function for the moeplayer to get the redirected url. Allows us to just check the moeplayer link once.
 */
indexbot.overrides.loadVideoOverride = function() {
    if (!indexbot.settings.autoAnswer && !indexbot.settings.answerNotifications) {
        indexbot.overrides.originalloadVideo.bind(this)()
        return
    }
    let videourl = "https://animemusicquiz.com" + this.getVideoUrl();
    indexbot.checkMoeRedirect(videourl, function(url) {
        this.player.src(url)
    }.bind(this));
}

/**
 * Override for a servise (sic) function that checks if more than 1 listener are on "quiz next video info".
 */
indexbot.overrides.getVolumeAtMax = function() {
    if (indexbot.controller.dangerMode()) {
        return false;
    }
    return indexbot.overrides.originalgetVolumeAtMax();
}

/**
 * CurrentVideoPlaying override. AMQ has a sneaky little bit that checks if the video is currently obscured. We want ot override it so it's alays yes.
 * It only seems to get checked when submitting an answer which should only be done while the video is obscured so it should be fine to do this?
 * I'm sure we'll find out
 */
indexbot.overrides.currentVideoPlayingOverride = function() {
    if (!indexbot.settings.autoRevealVideo) {
        return indexbot.overrides.originalCurrentVideoPlaying.bind(quizVideoController)()
    }
    return true
}

indexbot.overrides.overrideFunctions = function() {
    indexbot.overrides.originalCurrentVideoPlaying = QuizVideoController.prototype.currentVideoPlaying
    indexbot.overrides.originalgetVolumeAtMax = getVolumeAtMax;
    indexbot.overrides.originalloadVideo = MoeVideoPlayer.prototype.startLoading

    QuizVideoController.prototype.currentVideoPlaying = indexbot.overrides.currentVideoPlayingOverride
    getVolumeAtMax = indexbot.overrides.getVolumeAtMax;
    MoeVideoPlayer.prototype.startLoading = indexbot.overrides.loadVideoOverride

    //adding settings
    indexbot.html.addSetting("DANGER: Auto Answer", indexbot.html.input_types.CHECKBOX, indexbot.data.settingTable, "autoAnswer");
    indexbot.html.addSetting("DANGER: Answer Notifications", indexbot.html.input_types.CHECKBOX, indexbot.data.settingTable, "answerNotifications");
    indexbot.html.addSetting("DANGER: Reveal Video", indexbot.html.input_types.CHECKBOX, indexbot.data.settingTable, "autoRevealVideo");
}

indexbot.overrides.setupAfkTimeoutOverride = function() {
    if(!indexbot.settings.killAFKTimer) {
        return indexbot.overrides.originalSetupAfkTimeout.apply(afkKicker);
    }
    clearTimeout(afkKicker.afkWarningTimeout);
}

indexbot.overrides.setupHostTimeoutOverride = function() {
    if(!indexbot.settings.killAFKTimer) {
        return indexbot.overrides.originalSetupHostTimeout.apply(afkKicker);
    }
    clearTimeout(afkKicker.hostAfkWarningTimeout);
}

indexbot.overrides.killAFKTimer = function() {
    indexbot.overrides.originalSetupHostTimeout = afkKicker.setupHostTimeout
    indexbot.overrides.originalSetupAfkTimeout = afkKicker.setupAfkTimeout

    afkKicker.setupHostTimeout = indexbot.overrides.setupHostTimeoutOverride;
    afkKicker.setupAfkTimeout = indexbot.overrides.setupAfkTimeoutOverride;

    indexbot.html.addSetting("Kill AFK Timer", indexbot.html.input_types.CHECKBOX, indexbot.data.settingTable, "killAFKTimer");
}

// -----Listeners-----
indexbot.listeners = {}

indexbot.listeners.initAnilistLinkListener = function() {
    indexbot.data.anilistLinkListener = new Listener("answer results", function(results) {
        if (!indexbot.settings.anilistLink) {
            return;
        }
        let songData = results.songInfo;
        let anime = indexbot.controller.getAnimeName(songData.animeNames);
        const DOM_ELEMENT = $("#qpAnimeName");
        const ANILIST_LINK = "https://anilist.co/anime/";
        indexbot.anilist.queries.getAnilistAnimeID(anime, function(id) {
            const animeURL = ANILIST_LINK + id;
            indexbot.html.insertAnilistIntoDomElement(animeURL, anime, DOM_ELEMENT);
        });
    }.bind(this));
    indexbot.data.anilistLinkListener.bindListener();

    //adding settings
    indexbot.html.addSetting("Insert Anilist Link", indexbot.html.input_types.CHECKBOX, indexbot.data.settingTable, "anilistLink");
}

indexbot.listeners.initAnimeAsBGListener = function() {
    indexbot.data.animeAsBGRemoveListener = new Listener("play next song", function(data) {
        indexbot.styles.clearGameBG();
    }.bind(this));
    indexbot.data.animeAsBGRemoveListenerQuizEnd = new Listener("quiz over", function(result) {
        indexbot.styles.clearGameBG();
    }.bind(this));
    indexbot.data.animeAsBGRemoveListenerGameStart = new Listener("Game Starting", function(result) {
        indexbot.styles.clearGameBG();
    }.bind(this));
    indexbot.data.animeAsBGListener = new Listener("answer results", function(results) {
        if (!indexbot.settings.animeAsBackground) {
            return
        }
        let songData = results.songInfo;
        let anime = indexbot.controller.getAnimeName(songData.animeNames);
        indexbot.anilist.queries.getAnilistAnimeID(anime, function(id) {
            indexbot.anilist.queries.getAnilistImage(id, function(img) {
                indexbot.styles.setGameBG(img);
            });
        });
    });

    indexbot.data.animeAsBGRemoveListener.bindListener();
    indexbot.data.animeAsBGRemoveListenerQuizEnd.bindListener();
    indexbot.data.animeAsBGListener.bindListener();
    indexbot.data.animeAsBGRemoveListenerGameStart.bindListener();

    indexbot.html.addSetting("Anime as Background", indexbot.html.input_types.CHECKBOX, indexbot.data.settingTable, "animeAsBackground");
}

indexbot.listeners.initSongLogListener = function() {
    indexbot.data.songLogListener = new Listener("answer results", function(results) {
        indexbot.utility.debug(results)
        let player_data = results.players
        let songInfo = results.songInfo;
        let songName = songInfo.songName;
        let animeName = indexbot.controller.getAnimeName(songInfo.animeNames);
        let songDataObject = new SongData(songName, animeName);
        let myPlayerID;
        let myResults;
        let correct;
        for (const player in quiz.players) {
            if (quiz.players[player]._name == selfName) {
                myPlayerID = quiz.players[player].gamePlayerId
                for (const p in player_data) {
                    if (player_data[p].gamePlayerId == myPlayerID) {
                        myResults = player_data[p]
                    }
                }
            }
        }
        let urls = []
        for (const host in songInfo.urlMap) {
            for (const url in songInfo.urlMap[host]) {
                realurl = songInfo.urlMap[host][url];
                let song = {
                    url: realurl,
                    anime: animeName
                }
                urls.push(song)
            }
        }
        indexbot.database.bulkStoreSongs(urls)
        if (myPlayerID == undefined) {
            return
        }

        correct = myResults.correct
        indexbot.database.sawSong(songDataObject, correct);

    }.bind(this));
    indexbot.data.songLogListener.bindListener()
}

indexbot.listeners.initSongGuessPercentageListener = function() {
    indexbot.data.songGuessPercentageListener = new Listener("answer results", function(results) {
        if(!indexbot.settings.displaySongPercentage) {
            return;
        }
        let songInfo = results.songInfo;
        let songName = songInfo.songName;
        indexbot.database.getSeenSong(songName, function(data) {
            let percentage;
            if (data == null) {
                percentage = "New!"
                indexbot.html.insertSongGuessPercentage(percentage);
                return;
            }
            percentage = data.timesCorrect / data.timesSeen;
            percentage *= 100
            percentage = "" + percentage.toFixed(2) + "%";
            indexbot.html.insertSongGuessPercentage(percentage);
        })
    }.bind(this));
    indexbot.data.songGuessPercentageListener.bindListener();

    indexbot.html.addSetting("Show Song Guess Rate", indexbot.html.input_types.CHECKBOX, indexbot.data.settingTable, "displaySongPercentage")
}

indexbot.listeners.initEndOfRoundReportListener = function() {
    indexbot.data.endOfRoundReportListenerNewRound = new Listener("quiz ready", function(payload) {
        indexbot.data.roundData = new RoundData();
        indexbot.html.clearRoundReportEntries();
    }.bind(this));
    indexbot.data.endOfRoundReportListenerAnswersRevealed = new Listener("answer results", function(payload) {
        let answer = quiz.answerInput.quizAnswerState.$INPUT.prop("value");
        let songInfo = payload.songInfo;
        let payloadPlayers = payload.players;
        let [playerid, playerResults] = indexbot.controller.findPlayerData(selfName, quiz.players, payloadPlayers);
        let correct = playerResults.correct;
        let animeName = indexbot.controller.getAnimeName(songInfo.animeNames);
        let videoHost = quizVideoController.getCurrentHost();
        let videoRes = quizVideoController.getCurrentResolution();
        let url = songInfo.urlMap[videoHost][videoRes];
        let entry = indexbot.data.roundData.addSong(animeName, url, answer, correct);
        indexbot.data.roundData.score = playerResults.score;
        indexbot.data.roundData.placement = playerResults.position;
        indexbot.data.roundData.seen += 1;
        indexbot.data.roundData.correct += correct ? 1 : 0;
        indexbot.html.addRoundReportEntry(entry);

    }.bind(this));

    indexbot.data.endOfRoundReportListenerAnswersRevealed.bindListener();
    indexbot.data.endOfRoundReportListenerNewRound.bindListener();
}

/**
 * Begins the database connection and performs all other first time setup necessities such as overriding necessary CSS rules.
 * 
 */
indexbot.start = async function() {
    indexbot.styles.addStaticStyles();
    await indexbot.database.beginDatabaseConnection().then(indexbot.database.loadsettings).catch((error) => indexbot.utility.alert(error));
    //await indexbot.database.loadsettings();
    indexbot.html.buildMainSettingsPage();
    indexbot.styles.addDynamicStyles();
    indexbot.styles.addAnimeImageBGDiv();
    indexbot.listeners.initAnilistLinkListener()
    indexbot.listeners.initAnimeAsBGListener();
    indexbot.listeners.initSongLogListener();
    indexbot.listeners.initSongGuessPercentageListener();
    indexbot.controller.initRoundReport();
    indexbot.controller.quickJoinSetup();
    indexbot.overrides.killAFKTimer();
    indexbot.overrides.overrideFunctions();
    indexbot.controller.addDebugOption();
    indexbot.utility.alert("Indexbot Loaded Successfully!", "success");
}
indexbot.start()